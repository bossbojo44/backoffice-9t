﻿var SecurityModel = {

    GenerateKeyPair: function(keySize) {
        var rsa = new System.Security.Cryptography.RSACryptoServiceProvider(keySize);
        var keys = { publicKey: null, privateKey: null };
        try {
            keys.publicKey = rsa.ToXmlString(false);
            keys.privateKey = rsa.ToXmlString(true);
        } catch (ex) { console.log(ex); }
        return keys;
    },

    Encrypt: function(publicKey, plainText) {
        var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
        try {
            rsa.FromXmlString(publicKey);
            var encryptText = rsa.Encrypt(System.Text.Encoding.UTF8.GetBytes(plainText), true);
            return System.Convert.ToBase64String(encryptText);
        } catch (ex) { console.log(ex); }
        return null;
    },

    Decrypt: function(privateKey, chiperText) {
        var rsa = new System.Security.Cryptography.RSACryptoServiceProvider();
        try {
            rsa.FromXmlString(privateKey);
            var encryptBytes = System.Convert.FromBase64String(chiperText);
            return System.Text.Encoding.UTF8.GetString(rsa.Decrypt(encryptBytes, true));
        } catch (ex) {
            console.log(ex);
            return null;
        }
    }

};