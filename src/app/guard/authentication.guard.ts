import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { HttpService, ResponseModel } from '../services/http.service';
import { Injectable } from '@angular/core';
import { UrlConfig } from '../configs/url.config';
import { AuthenticationService } from '../services/authentication.service';
@Injectable()
export class AuthenticationGuard implements CanActivate {
    constructor(private http: HttpService, private router: Router, private authService: AuthenticationService) { }
    canActivate(route, state): boolean | Observable<boolean> {
        this.http
            .requestGet('/api/Authorization')
            .subscribe(res => { }, err => this.router.navigate(['/', UrlConfig.Signin]));
        return true;
    }
}