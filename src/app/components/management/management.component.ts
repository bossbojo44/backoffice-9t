import { ManagementService } from './management.service';

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.scss'],
  providers: [ManagementService]
})
export class ManagementComponent implements OnInit {
  public policyDataAll: Policy[];
  public updateJson;
  constructor(private servicePolicy: ManagementService) {
    this.readPolicyPrice();
    //(<HTMLInputElement>document.getElementById('txtQNAP')).value = this.getPolicy[0].policy_price1.toString();
  }

  ngOnInit() {
  }

  readPolicyPrice(){
        this.servicePolicy.readPolicyPrice().subscribe((res: any) => {
          this.policyDataAll = [];
            for(let i:number = 0;i<res.data.length; i++)
              this.policyDataAll.push(new Policy(res.data[i].products_id, res.data[i].price));

            (<HTMLInputElement>document.getElementById('txtQNAP')).value = this.policyDataAll[1].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtHP')).value = this.policyDataAll[2].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtProcessor')).value = this.policyDataAll[5].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtMemory')).value = this.policyDataAll[6].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtIP')).value = this.policyDataAll[7].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtNetwork')).value = this.policyDataAll[8].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtVm')).value = this.policyDataAll[0].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtTraffic')).value = this.policyDataAll[9].policy_price1.toString();
           /* (<HTMLInputElement>document.getElementById('txtProEss')).value = this.policyDataAll[8].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtPro')).value = this.policyDataAll[9].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtEnEss')).value = this.policyDataAll[10].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtEn')).value = this.policyDataAll[11].policy_price1.toString();*/

            (<HTMLInputElement>document.getElementById('txtQNAP2')).value = this.policyDataAll[1].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtHP2')).value = this.policyDataAll[2].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtVm2')).value = this.policyDataAll[0].policy_price1.toString();

            (<HTMLInputElement>document.getElementById('txtProcessor2')).value = this.policyDataAll[5].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtMemory2')).value = this.policyDataAll[6].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtIP2')).value = this.policyDataAll[7].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtNetwork2')).value = this.policyDataAll[8].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtTraffic2')).value = this.policyDataAll[9].policy_price1.toString();

           /* (<HTMLInputElement>document.getElementById('txtProEss2')).value = this.policyDataAll[8].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtPro2')).value = this.policyDataAll[9].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtEnEss2')).value = this.policyDataAll[10].policy_price1.toString();
            (<HTMLInputElement>document.getElementById('txtEn2')).value = this.policyDataAll[11].policy_price1.toString();*/
        });
  }

  showMain(){
    (<HTMLInputElement>document.getElementById('main')).classList.remove('invisible');
    (<HTMLInputElement>document.getElementById('backup')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('replication')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('nakivo')).classList.add('invisible');
  }

  showBackup(){
    (<HTMLInputElement>document.getElementById('main')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('backup')).classList.remove('invisible');
    (<HTMLInputElement>document.getElementById('replication')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('nakivo')).classList.add('invisible');
  }

  showReplication(){
    (<HTMLInputElement>document.getElementById('main')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('backup')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('replication')).classList.remove('invisible');
    (<HTMLInputElement>document.getElementById('nakivo')).classList.add('invisible');
  }

  showNakivo(){
    (<HTMLInputElement>document.getElementById('main')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('backup')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('replication')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('nakivo')).classList.remove('invisible');
  }

  submitUpdate(){
    this.servicePolicy.updatePolicyPrice(this.updateJson).subscribe((res: any) =>{
      this.readPolicyPrice();
    });  
  }

  updateQNAP(data: any){
    this.updateJson = {product_id:1102, price: parseFloat(data.value)};  
    var msg = "Change for QNAP to be $"+data.value+" per 500GB";
    document.getElementById('option').innerHTML = msg;
  }

  updateHP(data: any){
    this.updateJson = {product_id:1103, price: parseFloat(data.value)};  
    var msg = "Change for HP to be $"+data.value+" per 500GB";
    document.getElementById('option').innerHTML = msg;
  }

  updateProcessor(data: any){
    this.updateJson = {product_id:1203, price: parseFloat(data.value)};   
    var msg = "Change for Processor to be $"+data.value+" per 1Ghz";
    document.getElementById('option').innerHTML = msg;
  }

  updateMemory(data: any){
    this.updateJson = {product_id:1204, price: parseFloat(data.value)};  
    var msg = "Change for Memory to be $"+data.value+" per 4GB";
    document.getElementById('option').innerHTML = msg;
  }
  
  updateIPAddress(data: any){
    this.updateJson = {product_id:1205, price: parseFloat(data.value)};  
    var msg = "Change for 1IP Address to be $"+data.value;
    document.getElementById('option').innerHTML = msg;
  }

  updateNetwork(data: any){
    this.updateJson = {product_id:1206, price: parseFloat(data.value)};  
    var msg = "Change for 1Network to be $"+data.value;
    document.getElementById('option').innerHTML = msg;
  }

  updateVm(data: any){
    this.updateJson = {product_id:1101, price: parseFloat(data.value)};  
    var msg = "Change for  1 Virtual machines Licenses to be $"+data.value;
    document.getElementById('option').innerHTML = msg;
  }

  updateTraffic(data: any){
    this.updateJson = {product_id:1207, price: parseFloat(data.value)};    
    var msg = "Change for Internet Traffic to be $"+data.value+" per 250GB";
    document.getElementById('option').innerHTML = msg;
  }

  updateProEssential(data: any){
    this.updateJson = {Id: 9, policy_id:58, policy_price1: parseFloat(data.value), currency: "USD"}; 
    var msg = "Change for Pro Essentials to be $"+data.value;
    document.getElementById('option').innerHTML = msg; 
  }

  updateEnterpriseEssential(data: any){
    this.updateJson = {Id: 11, policy_id:60, policy_price1: parseFloat(data.value), currency: "USD"}; 
    var msg = "Change for Enterprise Essentials to be $"+data.value;
    document.getElementById('option').innerHTML = msg; 
  }

  updatePro(data: any){
    this.updateJson = {Id: 10, policy_id:59, policy_price1: parseFloat(data.value), currency: "USD"};  
    var msg = "Change for Pro to be $"+data.value;
    document.getElementById('option').innerHTML = msg;
  }

  updateEnterprise(data: any){
    this.updateJson = {Id: 12, policy_id:61, policy_price1: parseFloat(data.value), currency: "USD"};  
    var msg = "Change for Enterprise to be $"+data.value;
    document.getElementById('option').innerHTML = msg;
  }

}
class Policy{
  constructor(id:number, po_price:number){
    this.Id = id;
    this.policy_price1 = po_price;
  }
  public Id:number;
  public policy_price1: number;
}
