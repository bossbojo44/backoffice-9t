import { Injectable } from '@angular/core';
import { HttpService } from './../../services/http.service';

@Injectable()
export class ManagementService {

  constructor(private http: HttpService) { }
  readPolicy(){
     return this.http.requestPost('api/policy-all',null);
   }

  readPolicyPrice(){
     return this.http.requestGet('api/policy-all');
   }

  addPolicyPrice(requestData: any){
     return this.http.requestPost('api/add-policyPrice',requestData);
   }

  updatePolicyPrice(updateData: any){
     return this.http.requestPost('api/update-policyPrice',updateData);
   }
}
