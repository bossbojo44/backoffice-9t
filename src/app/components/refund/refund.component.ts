import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RefundService } from './refund.service';


declare let $;
@Component({
  selector: 'app-refund',
  templateUrl: './refund.component.html',
  styleUrls: ['./refund.component.scss'],
  providers: [RefundService]
})
export class RefundComponent implements OnInit {
  formFilterRefund: FormGroup;
  formFilterProcessed: FormGroup;
  UrlCurrentState: any = [];
  public dataRefund: any = [];
  public dataRefundProcessed: any = [];
  message: any;
   datastatus = {
    pa: 'Paid',
    ac: 'Active',
    rm: 'Remove',
    up: 'Unpaid',
    au: 'Unpaid_active',
    ov: 'Overdue',
    ao: 'overdue_active'

  } 
   datastatusProcessed = {
    pa: 'Paid',
    ac: 'Active',
    rm: 'Remove',
    up: 'Unpaid',
    au: 'Unpaid_active',
    ov: 'Overdue',
    ao: 'overdue_active'
  }

  constructor(builder: FormBuilder, private service: RefundService) {
    this.formFilterRefund = builder.group({
      cust_id : [''],
      email: [''],
      invoice_id: [''],
      pck_id: [''],
      pck_type_name: ['']
    });

    this.formFilterProcessed = builder.group({
      cust_id : [''],
      email: [''],
      invoice_id: [''],
      pck_id: [''],
      pck_type_name: ['']
    });

    this.Initialize();
  }

  ngOnInit() {
    //this.RefundAll();
    //this.btnPackagesFilters();
    //this.btnPackagesFiltersProcessed();
  }

  refund(id,vcc_id){
      $('#'+id).remove();
    let data = {
      pck_id:id,
      vcc_id:vcc_id
    }

      this.service.PostRefund(data).subscribe((res:any) => {
        this.message = res.data.Message;
    }, err => {
            
            this.message = err.json().Message;
    });
  }

  btnPackagesFilters() {
    let dataPackages = {
      cust_id: this.formFilterRefund.controls.cust_id.value,
      email: this.formFilterRefund.controls.email.value,
      invoice_id: this.formFilterRefund.controls.invoice_id.value,
      pck_id: this.formFilterRefund.controls.pck_id.value,
      pck_type_name: this.formFilterRefund.controls.pck_type_name.value
    }

    this
      .service
      .PostRefundFilters(dataPackages)
      .subscribe(res => {
         this.dataRefund = res;
        
      });
  }

   btnPackagesFiltersProcessed() {
    let dataPackages = {
      cust_id: this.formFilterProcessed.controls.cust_id.value,
      email: this.formFilterProcessed.controls.email.value,
      invoice_id: this.formFilterProcessed.controls.invoice_id.value,
      pck_id: this.formFilterProcessed.controls.pck_id.value,
      pck_type_name: this.formFilterProcessed.controls.pck_type_name.value
    }

    this
      .service
      .PostPackagesFiltersProcessed(dataPackages)
      .subscribe(res => {
       this.dataRefundProcessed = res;
      });
  }

 RefundAll() {
    this.service.AllRefund().subscribe(res => {
      this.dataRefund = res;
      this.btnPackagesFiltersProcessed();
    });
  }

  deleteRefund(id,vcc_id){
 $('#'+id).remove();
    let data = {
      pck_id:id,
      vcc_id:vcc_id
    }

      this.service.PostdeleteRefund(data).subscribe((res:any) => {
        this.message = res.data.Message;
    }, err => {
            
            this.message = err.json().Message;
    });
  }



  Initialize() {
    this.UrlCurrentState = 'Requests';
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  watch_nav(Url: string) {
    this.UrlCurrentState = Url;
    $('#nav_list').toggleClass('active');
    $('.pushmenu-left').toggleClass('pushmenu-open');
    $("#nav_list").hasClass('active')
      ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
      : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
  }


}
