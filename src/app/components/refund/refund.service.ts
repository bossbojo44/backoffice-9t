import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class RefundService {

  constructor(private http:HttpService) { }

 AllRefund(){
    return this.http.requestPost('api/refund-all', null);
  }
 PostRefundFilters(model: any) {
    return this.http.requestPost('api/refund-filters', (model));
  }

  PostPackagesFiltersProcessed(model: any) {
    return this.http.requestPost('api/refund-filters-processed', (model));
  }

   PostRefund(model:any){
    return this.http.requestPost('api/refund-ByPck-id',model);
  }
   PostdeleteRefund(model:any){
    return this.http.requestPost('api/Delete-refund-ByPck-id',model);
  }

}
