import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class UsersService {

  constructor(private http:HttpService) { }
PostUserAll(){
    return this.http.requestPost('api/user-all',null);
  }

PostPositionAll(){
    return this.http.requestPost('api/position-all',null);
  }

  PostEditUser(model:any){
    return this.http.requestPost('api/update-user',model);
  }
  PostBlockUser(id:number){
    console.log(id);
    return this.http.requestPost('api/block-user?ID='+id,null);
  }
  PostDelUser(id:number){
    console.log(id);
    return this.http.requestPost('api/del-user?ID='+id,null);
  }

  PostfilterDataUser(model:any){
    return this.http.requestPost('api/find-user',model);
  }

}
