import { Component, OnInit } from '@angular/core';
import { UsersService } from './users.service';
declare let $;
@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  providers: [UsersService]
})
export class UsersComponent implements OnInit {
  UserDataAlls: any = [];
  PositionAll: any = [];
  role: any = [];
  statusUser: any = [];
  PositionUser: any = [];
  fristnametext: any = [];
  lastnametext: any = [];
  usernametext: any = [];
  passwordtext: any = [];
  Idtext:any=[];
  rolee: any = '';

  constructor(private service: UsersService) { }
  loadDataUsers() {
    this.service.PostUserAll().subscribe((res: any) => {
    this.role = [];
    this.statusUser = [];
    this.fristnametext =[];
    this.lastnametext = [];
    this.usernametext = [];
    this.Idtext = [];
  

      this.UserDataAlls = res;
      res.data.forEach(item => this.role.push(item.emp_permission));
      res.data.forEach(item => this.statusUser.push(item.emp_status));
      res.data.forEach(item => this.fristnametext.push(item.emp_fristname));
      res.data.forEach(item => this.lastnametext.push(item.emp_lastname));
      res.data.forEach(item => this.usernametext.push(item.emp_username));
      res.data.forEach(item => this.Idtext.push(item.emp_id));
    });
  }

fliterDataUser(){
let datauser = {
emp_id:$('#filterId').val(),
emp_username:$('#filterUsername').val(),
emp_fristname:$('#filterName').val(),
emp_lastname:$('#filterFamily').val(),
emp_position:$('#filterPosition').find(":selected").val(),
emp_status:$('#filterStatus').find(":selected").val()
}
  this.service.PostfilterDataUser(datauser).subscribe((res: any) =>{
    this.role = [];
    this.statusUser = [];
    this.fristnametext =[];
    this.lastnametext = [];
    this.usernametext = [];
    this.passwordtext = [];
    this.Idtext = [];

       this.UserDataAlls = res;
      res.data.forEach(item => this.role.push(item.emp_permission));
      res.data.forEach(item => this.statusUser.push(item.emp_status));
      res.data.forEach(item => this.fristnametext.push(item.emp_fristname));
      res.data.forEach(item => this.lastnametext.push(item.emp_lastname));
      res.data.forEach(item => this.usernametext.push(item.emp_username));
      res.data.forEach(item => this.passwordtext.push(item.emp_password));
      res.data.forEach(item => this.Idtext.push(item.emp_id));
  });
}


  statusAdd() {
    this.loadDataPosition();
    this.fliterDataUser();
  }

  loadDataPosition() {
    this.service.PostPositionAll().subscribe((ress: any) => {
      this.PositionAll = ress;
      ress.data.forEach(itemm => this.PositionUser.push(itemm.position_name));
    });
  }

  ngOnInit() {
    this.rolee = localStorage.getItem('Role');
    this.loadDataPosition();
  }
  show() {
    $('#adduserdialog').modal()
  }

  clickEditIcons(id: number) {
    $(".show-tr-" + id).css("display", "none");
    $(".none-tr-" + id).css("display", "table-row");
  }

  clickBlockIcons(user: any,index){
    //this.statusUser[index] = 'd';
    if(this.statusUser[index] =='b'){
      this.statusUser[index] = 'a';
    }else{
      this.statusUser[index] = 'b';
    }
    this.service.PostBlockUser(user.emp_id).subscribe(res =>{
    });
  }

  clickDeleteIcons(user: any,index){
    this.service.PostDelUser(user.emp_id).subscribe(res =>{
    });
     $(".show-tbody-" + user.emp_id).css("display", "none");
  }

  clickSaveIcons(user: any) {

    user.position_name = $('#gettextposition-'+user.emp_id).find(":selected").text();
    let dataAdduser = {
      emp_id:user.emp_id,
      emp_username:$('#gettextusername-'+user.emp_id).val(),
      emp_password:'',
      emp_fristname:$('#gettextfristname-'+user.emp_id).val(),
      emp_lastname:$('#gettextlastname-'+user.emp_id).val(),
      emp_permission:$('#gettextpermission-'+user.emp_id).find(":selected").val(),
      emp_position:$('#gettextposition-'+user.emp_id).find(":selected").val(),
      emp_status:$('#gettextstatus-'+user.emp_id).find(":selected").val(),
    }

    this.service.PostEditUser(dataAdduser).subscribe(res =>{
    });

    $(".show-tr-" + user.emp_id).css("display", "table-row");
    $(".none-tr-" + user.emp_id).css("display", "none");

  }

}
