import { TestBed, inject } from '@angular/core/testing';

import { SigninpinService } from './signinpin.service';

describe('SigninpinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SigninpinService]
    });
  });

  it('should ...', inject([SigninpinService], (service: SigninpinService) => {
    expect(service).toBeTruthy();
  }));
});
