import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SigninpinComponent } from './signinpin.component';

describe('SigninpinComponent', () => {
  let component: SigninpinComponent;
  let fixture: ComponentFixture<SigninpinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SigninpinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SigninpinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
