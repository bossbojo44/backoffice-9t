import { SharedService } from './../../services/shared.service';
import { VerifyService } from './../../services/verify.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SigninpinService } from './signinpin.service';
import { Router } from '@angular/router';
import { UrlConfig } from '../../configs/url.config';
import { AuthenticationService } from '../../services/authentication.service';
import { SignalRUserOnlineService } from '../../services/SignalR.UserOnine.service';

declare let $;
@Component({
    selector: 'app-signinpin',
    templateUrl: './signinpin.component.html',
    styleUrls: ['./signinpin.component.scss'],
    providers: [SigninpinService]
})
export class SigninpinComponent implements OnInit {
    formsigninpin: FormGroup;
    message: any;
    constructor(private userOnlne: SignalRUserOnlineService, private global: SharedService, builder: FormBuilder, private service: SigninpinService, private verifyService: VerifyService, private authenticated: AuthenticationService, private router: Router) {

        this.formsigninpin = builder.group({
            pin: builder.array([
                [''],
                [''],
                [''],
                [''],
                [''],
                ['']
            ])
        });
    }

    ngOnInit() {
        setTimeout(() => {
            if (this.verifyService.getTempData == null) {
                this.router.navigate(['/', UrlConfig.Signin]);
            }
        }, 500);
        setTimeout(() => {
            $(".inputs-0").focus();
        }, 500);
    }
    ok() {
        let model = this.formsigninpin.value;
        let data = {
            TempData: this.verifyService.getTempData,
            PIN: model.pin.join('')
        }

        this.service.PostsigninCon(data).subscribe((res: any) => {
            this.global.EmpId = res.data.EmpId;
            this.verifyService.setTempData(null);

            localStorage.setItem('EmpId', this.global.EmpId.toString());
            localStorage.setItem('Role', res.data.Role);
            localStorage.setItem('Username', res.data.Username);
            this.authenticated.setAuthenticated(res.data.Token);
            
            /*this.userOnlne.getConnection.then(() => {
                this.userOnlne.server.userConnect(res.data.Token);
            });*/

            setTimeout(() => {
                this.router.navigate(['/', UrlConfig.Clients]);
            }, 1000);

        }, err => {

            this.message = err.json().Message;
        });

    }

    onKeyin(e, index) {
        var key = e.keyCode || e.charCode;
        if (key == 8) {
            var sumidndex = index - 1;
            if (sumidndex >= 0 && sumidndex <= 5) {
                $('.inputs-' + sumidndex).focus();
            }
        }
        else {
            var sumindex = index + 1;
            $('.inputs-' + sumindex).focus();
        }
    }

    onKeydow(e, index) {
        var key = e.keyCode || e.charCode;
        if (key == 8) {
            var sumidndex = index - 1;
            if (sumidndex >= 0 && sumidndex <= 5) {
                $('.inputs-' + sumidndex).focus();
            }
        }
    }

    get getFormArray() {
        return (<FormArray>this.formsigninpin.get('pin')).controls;
    }
}