import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class SigninpinService {

  constructor(private http:HttpService) { }

  PostsigninCon(model:any){
    return this.http.requestPost('api/signin/pin',model);
  }

}
