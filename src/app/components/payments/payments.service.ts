import { HttpService } from './../../services/http.service';
import { Injectable } from '@angular/core';

@Injectable()
export class PaymentsService {

  constructor(private http:HttpService) { }

  public AllPaymentsTx(){
    return this.http.requestPost('api/payments-all', null);
  }

  public findPaymentsTx(filterModel){
    return this.http.requestPost('api/payments-filters', filterModel);
  }

  public loadPackageType(){
    return this.http.requestPost('api/package-type-all', null);
  }

  public invoiceViews(model){
    return this.http.requestPost("api/invoice-details", model);
  }

  public detailViews(model){
    return this.http.requestPost("api/payment-details", model);
  }

  GetveeamAll() {
    return this.http.requestGet('api/payment/getveeam');
  }

}
