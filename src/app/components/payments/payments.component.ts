import { element } from 'protractor';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PaymentsService } from './payments.service';
import { Component, OnInit } from '@angular/core';
declare let $;
@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
  providers: [PaymentsService]
})
export class PaymentsComponent implements OnInit {
  public dataPayments: any = [];
  public packagesAll: any = [];
  public InvoiceData: any = [];
  public PaymentData: any = [];
  PaymentVeeamAlls = {};

  public formPayments: FormGroup;
  InvoiceNumber: any = '0000000000';
  Issues: any = '00/00/0000';
  Due: any = '00/00/0000';
  SumTotal: number = 0;

  constructor(builder: FormBuilder, private service: PaymentsService) {
    this.formPayments = builder.group({
      PaymentNo: [""],
      PaymentStatus: [""],
      Name: [""],
      Family: [""],
      PackageStatus: [""]
    });
    this.formPayments.controls['PaymentStatus'].setValue(0);
    this.formPayments.controls['PackageStatus'].setValue(0);
  }

  resetData() {
    this.dataPayments = [];
    this.packagesAll = [];
    this.InvoiceData = [];
    this.PaymentData = [];
  }

  PaymentsVeeamAll(): void {
    this
      .service
      .GetveeamAll()
      .subscribe((res: any) => {
        this.PaymentVeeamAlls = res.data;
      });
  }

  ngOnInit() {
    this.loadPackageType();
    this.PaymentsVeeamAll();
  }

  public loadData(): void {
    this.service.AllPaymentsTx().subscribe(res => {
      this.dataPayments = res;
    });
  }

  public loadPackageType(): void {
    this.service.loadPackageType().subscribe(res => {
      this.packagesAll = res;
    });
  }

  public onSubmitFilter(): void {
    if (this.formPayments.valid) {
      this.service.findPaymentsTx(this.formPayments.value).subscribe(res => {
        this.dataPayments = res;
      });
    }
    else {
      this.loadData();
    }
  }

  public exportData(): void {
    let result: string = "Payment Records\r\nPayment No, Status, Type, Price, Package No, VM, Storage, Payment date, End date, Client ID, VCC login, Name, Family, Company\r\n";

    this.dataPayments.data.forEach(element => {
      result += (element.payment_id + ", " + element.pck_status + ", " + element.pck_type_name + ", " + element.total_price + ", "
        + element.pck_id + ", " + element.vm + ", " + element.storage + ", " + element.payment_dt + ", "
        + element.pck_end_dt + ", " + element.cust_id + ", " + element.username + ", " + element.firstname + ", "
        + element.lastname + ", " + element.company_name + "\r\n");
    });
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    element.setAttribute('download', 'Payment.csv');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  public onViewInvoice(paymentId: number, username: string): void {
    let data = {
      "Payment_Id": (paymentId || 0),
      "username": (username || '')
    };

     console.log(data);

    this.InvoiceData = [];
    this.service.invoiceViews(data).subscribe((res: any) => {

console.log('====================================');
console.log(res);
console.log('====================================');


      this.InvoiceData = (res.data || []);
      if (this.InvoiceData.length > 0) {
        this.SumTotal = 0;
        this.InvoiceNumber = this.InvoiceData[0].invoice_no;
        this.Issues = this.InvoiceData[0].issued;
        this.Due = this.InvoiceData[0].due;
        this.InvoiceData.forEach(e => {
          this.SumTotal += (e.storage_total_price + e.vm_total_price);
        });
        $('#invoices').modal('show');
      }
      else {
        this.InvoiceNumber = '0000000000';
        this.Issues = '00/00/0000';
        this.Due = '00/00/0000';
        this.SumTotal = 0;
      }
    });
  }

  public onViewDetails(txn_Id: string): void {
    let data = {
      "txnId": (txn_Id || 0)
    };
    this.PaymentData = [];
    this.service.detailViews(data).subscribe((res: any) => {
      if (res.data.length != 0) {
        this.PaymentData = res.data[0];
        if (this.PaymentData != null) {
          $('#detailviews').modal('show');
        }
      }
    });
  }
}
