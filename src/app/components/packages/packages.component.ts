import { URLCurrent } from './../../class/URLCurrent';
import { UrlConfig } from './../../configs/url.config';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { PackagesService } from './packages.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare let $;
@Component({ selector: 'packages', templateUrl: './packages.component.html', styleUrls: ['./packages.component.scss'], providers: [PackagesService] })
export class PackagesComponent implements OnInit, OnDestroy {
  public role: any = '';
  UrlCurrentState: string = 'Packages';
  Url = UrlConfig;
  PackagesDataAlls: any = [];
  PackagesTypeAlls: any = [];
  formPackages: FormGroup;
  private KeyUrl: string = 'packagecurrenturl';
  PackageVeeamAlls = {};
  datastatus = {
    pa: 'paid',
    ac: 'active',
    rm: 'remove',
    up: 'unpaid',
    au: 'unpaid_active',
    ov: 'overdue',
    ao: 'overdue_active',
    et: 'Enterprise'
  }

  constructor(builder: FormBuilder, private service: PackagesService) {
    this.formPackages = builder.group({ pck_id: [''], pck_type_id: ['0'], firstname: [''], lastname: [''], pck_status: [''] });
    this.Initialize();
  }

  ngOnDestroy() {
    //URLCurrent.RemoveByKey(this.KeyUrl);
  }

  Initialize(): void {
    let current = URLCurrent.get(this.KeyUrl);
    this.UrlCurrentState = current || 'Packages';

    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });

          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  btnPackagesFilters() {

    let aaa = this.formPackages.controls.pck_id.value;
    let id = 0;
    if (aaa == '') {
      id = this.formPackages.controls.pck_id.value;
    } else if (aaa == 0) {
      id = 1;
    } else {
      id = this.formPackages.controls.pck_id.value;
    }

    let dataPackages = {
      pck_id: id,
      pck_type_id: this.formPackages.controls.pck_type_id.value,
      firstname: this.formPackages.controls.firstname.value,
      lastname: this.formPackages.controls.lastname.value,
      pck_status: this.formPackages.controls.pck_status.value
    }

    this
      .service
      .PostPackagesFilters(dataPackages)
      .subscribe(res => {
        this.PackagesDataAlls = res;
        console.log(res)

        for (var i = 0; i < this.PackagesDataAlls.data.length; i++) {
          let aa = this.PackagesDataAlls.data[i].total_price;
          let bb = aa + ((aa * 7) / 100)
          let cc = bb.toFixed(2);
          this.PackagesDataAlls.data[i].total_price = cc
        }


      });
  }

  PackagesAll(): void {
    this
      .service
      .PostPackagesAll()
      .subscribe(res => {
        console.log(res)
        this.PackagesDataAlls = res;
      });
  }

  PackagesTypeAll(): void {
    this
      .service
      .PostPackgesTypeAll()
      .subscribe(res => {
        this.PackagesTypeAlls = res;
      });
  }


  PackagesVeeamAll(): void {
    this
      .service
      .GetveeamAll()
      .subscribe((res: any) => {
        this.PackageVeeamAlls = res.data;
      });
  }


  ngOnInit() {
    this.role = localStorage.getItem('Role');
    this.PackagesTypeAll();
    this.PackagesVeeamAll();
  }

  exportData(): void {
    let result: string = "List of Packages\r\nPackage No, Type, Price, Payment, Status, VM, Storage All, S" +
      "torage Used, Start Date, End Date, Clients ID, Name, Family, Company\r\n";

    this
      .PackagesDataAlls
      .data
      .forEach(element => {
        result += (element.pck_id + ", " + element.pck_type_name + ", " + element.total_price + ", " + element.pck_type_id + ", " + element.pck_status + ", " + element.vm + ", " + element.storage + ", , " + element.pck_start_dt + ", " + element.pck_end_dt + ", " + element.cust_id + ", " + element.firstname + ", " + element.lastname + ", " + element.company_name + "\r\n");
      });
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    element.setAttribute('download', 'Packages.csv');

    element.style.display = 'none';
    document
      .body
      .appendChild(element);

    element.click();

    document
      .body
      .removeChild(element);
  }

  openAvaible(): void {
    $('.menu-list').toggle('slide', {
      direction: 'left'
    }, 1000, () => {
      $('#sidebar-wrapper').css('width', 'unset');
    });
  }

  watch_nav(Url: string): void {
    URLCurrent.set(this.KeyUrl, Url);
    this.UrlCurrentState = Url;

    $('#nav_list').toggleClass('active');
    $('.pushmenu-left').toggleClass('pushmenu-open');
    $("#nav_list").hasClass('active')
      ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
      : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
  }

}