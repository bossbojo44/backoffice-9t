import { Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class PackagesService {
  constructor(private http: HttpService) { }

  PostPackagesAll() {
    return this.http.requestPost('api/package-all', null);
  }

  PostPackagesFilters(model: any) {
    return this.http.requestPost('api/package-filters', (model));
  }

  PostPackgesTypeAll() {
    return this.http.requestPost('api/package-type-all', null);
  }


  GetveeamAll() {
    return this.http.requestGet('api/packages/getveeam');
  }

  PostInvoice(model: any): Observable<InvoicesDetailModel> {
    return this.http.requestPostCustomResponse('/api/invoice-bill', model).map(res => res.data);
  }

}

export class InvoicesModel {
  invoice_no: string;
  payment_dt: Date;
  cart_total_price: number;
  payment_status: string;
}

export class InvoicesDetailModel {
  address: string;
  city: string;
  company_name: string;
  country: string;
  due: Date;
  email: string;
  firstname: string;
  invoice_no: string;
  issued: Date;
  lastname: string;
  pck_end_dt: Date;
  pck_start_dt: Date;
  phone_num: string;
  post_code: string;
  province: string;
  storage: number;
  storage_total_price: number;
  total_price: number;
  vm: number;
  vm_total_price: number;
}