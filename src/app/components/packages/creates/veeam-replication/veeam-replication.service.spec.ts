import { TestBed, inject } from '@angular/core/testing';

import { VeeamReplicationService } from './veeam-replication.service';

describe('VeeamReplicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VeeamReplicationService]
    });
  });

  it('should ...', inject([VeeamReplicationService], (service: VeeamReplicationService) => {
    expect(service).toBeTruthy();
  }));
});
