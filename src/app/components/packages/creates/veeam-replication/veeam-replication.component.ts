import { ValidationsEx } from './../../../../class/validation';
import { VeeamReplicationService } from './veeam-replication.service';
import { VeeamBackupService } from './../veeam-backup/veeam-backup.service';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'package-veeam-replication',
  templateUrl: './veeam-replication.component.html',
  styleUrls: ['./veeam-replication.component.scss'],
  providers: [VeeamReplicationService, VeeamBackupService]
})
export class VeeamReplicationComponent implements OnInit {
  veeambackuprep: FormGroup;
  public ClientData: any = [];
  public CalData: any = [];
  public isValid: boolean = false;

  constructor(private builder: FormBuilder, private veeamService: VeeamBackupService) {
    this.veeambackuprep = builder.group({
      ClientId: ['', [Validators.required, Validators.maxLength(40), ValidationsEx.specialcharacters]],
      StorageGB: [500],
      StorageBrand: [false],
      VMsQty: [1],
      Processor: [''],
      RAM: [''],
      IPAddress: [''],
      Networks: [''],
      InternetTraffic: [''],
      SendInvoice: [true]
    });

    this.CalPricePackages();
  }

  ngOnInit() {
    this.isValid = false;
  }

  public EnterClient(e: KeyboardEvent): void {
    if (e.keyCode == 13) this.getClientInfo();
  }

  public getClientInfo() {
    this.isValid = false;
    this.ClientData = [];
    let clientId = this.veeambackuprep.controls['ClientId'].value;
    this.veeamService.getClientInformation(clientId).subscribe((res: any) => {
      if (res.status === 200) {
        this.ClientData = res.data[0];
        this.isValid = true;
      }
    },
      err => {
        this.isValid = false;
        this.ClientData = [];
      });
  }

  public CalPricePackages(): void {
    let req = {
      "vm": this.veeambackuprep.controls['VMsQty'].value,
      "storage": this.veeambackuprep.controls['StorageGB'].value,
      "preniums_storage": this.veeambackuprep.controls['StorageBrand'].value
    };

    this.veeamService.PostCalPackage(req).subscribe((res: any) => {
      if (res.status === 200) {
        this.CalData = res.data;
      }
    },
      err => {
        this.CalData = [];
      });
  }

}