import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeeamReplicationComponent } from './veeam-replication.component';

describe('VeeamReplicationComponent', () => {
  let component: VeeamReplicationComponent;
  let fixture: ComponentFixture<VeeamReplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeeamReplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeeamReplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
