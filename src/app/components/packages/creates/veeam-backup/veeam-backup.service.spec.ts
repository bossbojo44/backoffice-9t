import { TestBed, inject } from '@angular/core/testing';

import { VeeamBackupService } from './veeam-backup.service';

describe('VeeamBackupService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [VeeamBackupService]
    });
  });

  it('should ...', inject([VeeamBackupService], (service: VeeamBackupService) => {
    expect(service).toBeTruthy();
  }));
});
