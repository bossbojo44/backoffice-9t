import { HttpService } from './../../../../services/http.service';
import { Injectable } from '@angular/core';

@Injectable()
export class VeeamBackupService {

  constructor(private http:HttpService) { }

  public getClientInformation(ClientId:number){
    let v = {
      "ClientId": ClientId
    };
    return this.http.requestPost('api/client/information', v);
  }

  public PostCalPackage(obj:any){   
    return this.http.requestPost('api/package-calculate', obj);
  }

  public PostAddClientVeeam(obj:any){   
    return this.http.requestPost('api/packages/create-veeam-backup', obj);
  }

}
