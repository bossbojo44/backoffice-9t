import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeeamBackupComponent } from './veeam-backup.component';

describe('VeeamBackupComponent', () => {
  let component: VeeamBackupComponent;
  let fixture: ComponentFixture<VeeamBackupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeeamBackupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeeamBackupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
