import { PackagesService, InvoicesDetailModel } from './../../packages.service';
import { VeeamBackupService } from './veeam-backup.service';
import { ValidationsEx } from './../../../../class/validation';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { RequestService } from '../../../request/request.service';

declare let $;

@Component({
  selector: 'package-veeam-backup',
  templateUrl: './veeam-backup.component.html',
  styleUrls: ['./veeam-backup.component.scss'],
  providers: [VeeamBackupService]
})
export class VeeamBackupComponent implements OnInit {
  veeambackup: FormGroup;
  previewStatus = false; //True is Enable ViewInvoice 
  items:any[]=[]
  public ClientData: any = [];
  public CalData: any = [];
  public isValid: boolean = false;
  private invoice_no: string = '';
  public invoiceDetail: InvoicesDetailModel;

  constructor(builder: FormBuilder, private veeamService: VeeamBackupService, private pckService: PackagesService,private requestService: RequestService) {
    this.items = this.requestService.getArray;
   // console.log(this.items);
    this.veeambackup = builder.group({
      ClientId: ['', [Validators.required, Validators.maxLength(40), ValidationsEx.specialcharacters]],
      StorageGB: [500],
      StorageBrand: [false],
      VMsQty: [1],
      SendInvoice: [true]
    });

    this.CalPricePackages();
  }

  ngOnInit() {
    this.isValid = false;
    this.invoice_no != '' ? this.previewStatus = true : this.previewStatus = false;
  }

  public EnterClient(e: KeyboardEvent) {
    if (e.keyCode == 13) { this.getClientInfo(); return false; };
  }

  public getClientInfo(): void {
    this.isValid = false;
    this.ClientData = [];
    let clientId = this.veeambackup.controls['ClientId'].value;
    this.veeamService.getClientInformation(clientId).subscribe((res: any) => {
      if (res.status === 200) {
        this.ClientData = res.data[0];
        this.isValid = true;
      }
    },
      err => {
        this.isValid = false;
        this.ClientData = [];
      });
  }
  

  public CalPricePackages(): void {
    let req = {
      "vm": this.veeambackup.controls['VMsQty'].value,
      "storage": this.veeambackup.controls['StorageGB'].value,
      "preniums_storage": this.veeambackup.controls['StorageBrand'].value
    };

    this.veeamService.PostCalPackage(req).subscribe((res: any) => {
      if (res.status === 200) {
        this.CalData = res.data;
      }
    },
      err => {
        this.CalData = [];
      });
  }

  public PreviewInvoice() {
    let req = {
      "invoice_no": this.invoice_no,
      "cust_id": this.veeambackup.controls['ClientId'].value
    };

    if (req.invoice_no == undefined) return;
    this.pckService.PostInvoice(req).subscribe(res => {
      this.invoiceDetail = res;
    });
    return this.invoiceDetail;
  }

  public submitCreateInvoice(): void {
     
    if (this.veeambackup.valid) {
      $(".xxxlode").show();
      let ClientId: number = this.veeambackup.controls['ClientId'].value;
      let StorageGB: number = this.veeambackup.controls['StorageGB'].value;
      let StorageBrand: boolean = this.veeambackup.controls['StorageBrand'].value;
      let VMsQty: number = this.veeambackup.controls['VMsQty'].value;
      let SendInvoice: boolean = this.veeambackup.controls['SendInvoice'].value;

      let req = new veeam_req(ClientId, StorageGB, StorageBrand, VMsQty, SendInvoice);

      this.veeamService.PostAddClientVeeam(req).subscribe((res: any) => {
         $(".xxxlode").hide();
        this.invoice_no = res.data[0].invoice_no;
        this.invoiceDetail = res.data;
        if (this.invoice_no != '') this.previewStatus = true;
      });
    }
  }
}

export class veeam_req {
  constructor(ClientId: number, StorageGB: number, StorageBrand: boolean, VMsQty: number, SendInvoice: boolean) {
    this.ClientId = ClientId;
    this.StorageGB = StorageGB;
    this.StorageBrand = StorageBrand;
    this.VMsQty = VMsQty;
    this.SendInvoice = SendInvoice;
  }
  ClientId: number;
  StorageGB: number;
  StorageBrand: boolean;
  VMsQty: number;
  SendInvoice: boolean;
}