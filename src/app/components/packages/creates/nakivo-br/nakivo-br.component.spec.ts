import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NakivoBrComponent } from './nakivo-br.component';

describe('NakivoBrComponent', () => {
  let component: NakivoBrComponent;
  let fixture: ComponentFixture<NakivoBrComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NakivoBrComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NakivoBrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
