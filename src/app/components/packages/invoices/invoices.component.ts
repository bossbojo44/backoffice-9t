import { PackagesService, InvoicesDetailModel } from './../packages.service';
import { UrlConfig } from './../../../configs/url.config';
import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
declare let $;
@Component({
  selector: 'bill-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  providers: [PackagesService]
})
export class InvoicesComponent implements OnChanges {

  data: any = [];
  data2: any = [];
  totol: number;
  invoices: string;
  issued: string;
  due: string;
  total: number = 0;
  Url = UrlConfig;
  FeesTotal: number = 0;
  totals: number = 0;
  @Input('invoice') invoice: InvoicesDetailModel;

  constructor() { }

  ngOnChanges() {
    if (!this.invoice) return;
    this.data = this.invoice;
    this.data2 = this.invoice[0];
    this.totol = this.invoice[0].cart_total_price;
    this.invoices = this.invoice[0].invoice_no;
    this.issued = this.invoice[0].issued;
    this.due = this.invoice[0].pck_end_dt;

    this.total = 0;
    this.FeesTotal = 0;
    this.totals = 0;

    this.data.forEach(el => {
      this.total = el.storage_total_price + el.vm_total_price + this.total;
      this.FeesTotal += el.fees;
      this.totals = el.storage_total_price + el.vm_total_price + el.fees + this.totals;
    });
  }

  onPrint() {
    print();
  }

}
