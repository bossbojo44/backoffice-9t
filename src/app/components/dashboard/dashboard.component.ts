import { NewOrders, NewClients, DashboardService } from './dashboard.service';
import { Component, OnInit } from '@angular/core';
import {GoogleChartComponent} from '../google-chart/google-chart.component';
declare let $;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  providers: [DashboardService, GoogleChartComponent]
})
export class DashboardComponent implements OnInit {
  public orders: NewOrders[];
  public clients: NewClients[];
  public successful: number = 0;
  public inProgress: number = 0;
  public notSuccessful: number = 0;

  /*pie_ChartData1 = [
                    ['Orders', 'Orders of period'],
                    ['Successful',     11],
                    ['In Progress',      2],
                    ['Not Successful',  5],
  ];*/
  public pie_ChartData1;
  public pie_ChartOptions1  = {
    legend: { maxLines: 7 },
    //title: 'SALES',
    width: 600,
    height: 400,
    //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
    colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
    chartArea:{width:'90%',height:'80%'},
    sliceVisibilityThreshold: 0,
    is3D: true,
    tooltip: { trigger: 'selection' },
  };


  constructor(private serviceDashboard: DashboardService, private googleChart: GoogleChartComponent) {
    
    this.orders = [];
    this.clients = [];
    this.readDashboard({quick: "hour"});
    this.Initialize();
    /*this.orders.push(new NewOrders(12345678, "PA23423423N232", "Backup", "1VM 500GB", 59, "Active"));
    this.orders.push(new NewOrders(12345678, "PA23423423N232", "Backup", "1VM 500GB", 59, "Active"));
    this.orders.push(new NewOrders(12345678, "PA23423423N232", "Backup", "1VM 500GB", 59, "Active"));
    this.orders.push(new NewOrders(12345678, "PA23423423N232", "Backup", "1VM 500GB", 59, "Active"));
    this.orders.push(new NewOrders(12345678, "PA23423423N232", "Backup", "1VM 500GB", 59, "Active"));

    this.clients.push(new NewClients(12345678, "Jack", "Daniels", "jack@gmail.com", 66966969696, 1));
    this.clients.push(new NewClients(12345678, "Jack", "Daniels", "jack@gmail.com", 66966969696, 1));
    this.clients.push(new NewClients(12345678, "Jack", "Daniels", "jack@gmail.com", 66966969696, 1));
    this.clients.push(new NewClients(12345678, "Jack", "Daniels", "jack@gmail.com", 66966969696, 1));
    this.clients.push(new NewClients(12345678, "Jack", "Daniels", "jack@gmail.com", 66966969696, 1));*/
   }

  ngOnInit() {
    this.orders = [];
    this.clients = [];
    this.readDashboard({quick: "hour"});
    $(".start_date").datetimepicker({minView:2, format: 'yyyy-mm-dd'});
    $(".end_date").datetimepicker({minView:2, format: 'yyyy-mm-dd'});
  }
Initialize() {
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });

          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }
  readDashboard(option: any){
    this.orders = [];
    this.clients = [];
    if(Object.keys(option).length == 1){
      this.serviceDashboard.searchQuick(option).subscribe((res:any)=> {
        console.log(res);
        this.successful = res.data.Success;
        this.inProgress = res.data.In_progress;
        this.notSuccessful = res.data.Not_success;
        if(res.data.Data.length < 1){
          this.pie_ChartData1 = [
                    ['Orders', 'Orders of period'],
                    ['Successful',     res.data.Success],
                    ['In Progress',      res.data.In_progress],
                    ['Not Successful',  res.data.Not_success],
                    ['Unpaid', res.data.Unpaid],
                    ['Unpaid(active)', res.data.Unpaid_active],
                    ['Overdue', res.data.Overdue],
                    ['Overdue(active)', res.data.Overdue_active],
                  ];

          (<HTMLInputElement>document.getElementById('emptyChart')).classList.remove('invisible');
          (<HTMLInputElement>document.getElementById('noData')).classList.remove('invisible');
        }
        else{
          this.pie_ChartData1 = [
                    ['Orders', 'Orders of period'],
                    ['Successful',    res.data.Success],
                    ['In Progress',     res.data.In_progress],
                    ['Not Successful',  res.data.Not_success],
                    ['Unpaid', res.data.Unpaid],
                    ['Unpaid(active)', res.data.Unpaid_active],
                    ['Overdue', res.data.Overdue],
                    ['Overdue(active)', res.data.Overdue_active],
                  ];
          (<HTMLInputElement>document.getElementById('emptyChart')).classList.add('invisible');
          (<HTMLInputElement>document.getElementById('noData')).classList.add('invisible');
        }
        
        var success = 0;
        var notSuccess = 0;
        var unpaid = 0;
        var unpaidActive = 0;
        var overdue = 0;
        var overdueActive = 0;
        var inProgress = 0;

        for(let i:number=0;i<res.data.Data.length;i++){

          var orderStatus = res.data.Data[i].status;
          var accStatus = res.data.Data[i].acc_status;
          if(orderStatus == "pa"){
            success+=res.data.Data[i].price;
            orderStatus = "Successful";
          }

          if(orderStatus == "rm"){
            notSuccess+=res.data.Data[i].price;
            orderStatus = "Not Successful";
          }

          if(orderStatus == "up"){
            unpaid+=res.data.Data[i].price;
            orderStatus = "Unpaid";
          }

          if(orderStatus == "ao"){
            overdueActive+=res.data.Data[i].price;
            orderStatus = "Overdue (active)";
          }

          if(orderStatus == "ov"){
            overdue+=res.data.Data[i].price;
            orderStatus = "Overdue";
          }

          if(orderStatus == "au"){
            unpaidActive+=res.data.Data[i].price;
            orderStatus = "Unpaid (active)";
          }

          if(orderStatus == "ac"){
            inProgress+=res.data.Data[i].price;
            orderStatus = "In progress";
          }

          if(accStatus == 0)
            accStatus = "Not Verified";

          if(accStatus == 1)
            accStatus = "Verified";

          if(accStatus == 2)
            accStatus = "Blocked";

          if(accStatus == 3)
            accStatus = "Freezed";

          this.orders.push(new NewOrders(res.data.Data[i].clientID_by_orders, 
                                        res.data.Data[i].transaction,
                                        res.data.Data[i].pck_type_name,
                                        res.data.Data[i].pck_details,
                                        res.data.Data[i].price,
                                        orderStatus));

          this.clients.push(new NewClients(res.data.Data[i].clientID_by_client,
                                          res.data.Data[i].firstname,
                                          res.data.Data[i].lastname,
                                          res.data.Data[i].email,
                                          res.data.Data[i].phone_num,
                                          accStatus));
        }

        this.googleChart.drawGraph(this.pie_ChartOptions1, "PieChart", this.pie_ChartData1, "pie_chart", success.toFixed(2), inProgress.toFixed(2), notSuccess.toFixed(2), unpaid.toFixed(2), unpaidActive.toFixed(2), overdue.toFixed(2), overdueActive.toFixed(2));
      });
    }
    else if(Object.keys(option).length == 2){
      this.serviceDashboard.searchCustom(option).subscribe((res:any)=> {
        console.log(res);
        this.successful = res.data.Success;
        this.inProgress = res.data.In_progress;
        this.notSuccessful = res.data.Not_success;
        
        if(res.data.Data.length < 1){
          this.pie_ChartData1 = [
                    ['Orders', 'Orders of period'],
                    ['Successful',     res.data.Success],
                    ['In Progress',      res.data.In_progress],
                    ['Not Successful',  res.data.Not_success],
                    ['Unpaid', res.data.Unpaid],
                    ['Unpaid(active)', res.data.Unpaid_active],
                    ['Overdue', res.data.Overdue],
                    ['Overdue(active)', res.data.Overdue_active],
                  ];
          console.log(res.data.Data.length);
          (<HTMLInputElement>document.getElementById('emptyChart')).classList.remove('invisible');
          (<HTMLInputElement>document.getElementById('noData')).classList.remove('invisible');
        }
        else{
          this.pie_ChartData1 = [
                    ['Orders', 'Orders of period'],
                    ['Successful',     res.data.Success],
                    ['In Progress',      res.data.In_progress],
                    ['Not Successful',  res.data.Not_success],
                    ['Unpaid', res.data.Unpaid],
                    ['Unpaid(active)', res.data.Unpaid_active],
                    ['Overdue', res.data.Overdue],
                    ['Overdue(active)', res.data.Overdue_active],
                  ];
          (<HTMLInputElement>document.getElementById('emptyChart')).classList.add('invisible');
          (<HTMLInputElement>document.getElementById('noData')).classList.add('invisible');
        }

        var success = 0;
        var notSuccess = 0;
        var unpaid = 0;
        var unpaidActive = 0;
        var overdue = 0;
        var overdueActive = 0;
        var inProgress = 0;

        for(let i:number=0;i<res.data.Data.length;i++){

          var orderStatus = res.data.Data[i].status;
          var accStatus = res.data.Data[i].acc_status;
          if(orderStatus == "pa"){
            success+=res.data.Data[i].price;
            orderStatus = "Successful";
          }

          if(orderStatus == "rm"){
            notSuccess+=res.data.Data[i].price;;
            orderStatus = "Not Successful";
          }

          if(orderStatus == "up"){
            unpaid+=res.data.Data[i].price;
            orderStatus = "Unpaid";
          }

          if(orderStatus == "ao"){
            overdueActive+=res.data.Data[i].price;
            orderStatus = "Overdue (active)";
          }

          if(orderStatus == "ov"){
            overdue+=res.data.Data[i].price;
            orderStatus = "Overdue";
          }

          if(orderStatus == "au"){
            unpaidActive+=res.data.Data[i].price;
            orderStatus = "Unpaid (active)";
          }

          if(orderStatus == "ac"){
            inProgress+=res.data.Data[i].price;
            orderStatus = "In progress";
          }

          if(accStatus == 0)
            accStatus = "Not Verified";

          if(accStatus == 1)
            accStatus = "Verified";

          if(accStatus == 2)
            accStatus = "Blocked";

          if(accStatus == 3)
            accStatus = "Freezed";


          this.orders.push(new NewOrders(res.data.Data[i].clientID_by_orders, 
                                        res.data.Data[i].transaction,
                                        res.data.Data[i].pck_type_name,
                                        res.data.Data[i].pck_details,
                                        res.data.Data[i].price,
                                        orderStatus));

          this.clients.push(new NewClients(res.data.Data[i].clientID_by_client,
                                          res.data.Data[i].firstname,
                                          res.data.Data[i].lastname,
                                          res.data.Data[i].email,
                                          res.data.Data[i].phone_num,
                                          accStatus));
        }

        this.googleChart.drawGraph(this.pie_ChartOptions1, "PieChart", this.pie_ChartData1, "pie_chart", success.toFixed(2), inProgress.toFixed(2), notSuccess.toFixed(2), unpaid.toFixed(2), unpaidActive.toFixed(2), overdue.toFixed(2), overdueActive.toFixed(2));
      });
    }
  }

  displayDate(start:any, end:any){
    let $menuLeft = $('.pushmenu-left');
    let $nav_list = $('#nav_list');
    $nav_list.toggleClass('active');
    $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
    $menuLeft.toggleClass('pushmenu-open');
    if(!start.value || !end.value){
      alert('Please fill Start and End date to be completely!');
    }
    else{
      var data = {from: start.value, to: end.value};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Period Report";
      this.readDashboard(data);
    }
  } 

  displayQuick(){
    let $menuLeft = $('.pushmenu-left');
    let $nav_list = $('#nav_list');
    $nav_list.toggleClass('active');
    $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
    $menuLeft.toggleClass('pushmenu-open');
    if((<HTMLInputElement>document.getElementById('lasthour')).checked == true){
      var data = {quick: "hour"};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Hourly Report";
      this.readDashboard(data);
    }
    else if((<HTMLInputElement>document.getElementById('lastday')).checked == true){
      var data = {quick: "day"};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Daily Report";
      this.readDashboard(data);
    }
    else if((<HTMLInputElement>document.getElementById('last3day')).checked == true){
      var data = {quick: "3day"};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Last 3 days Report";
      this.readDashboard(data);
    }
    else if((<HTMLInputElement>document.getElementById('lastweek')).checked == true){
      var data = {quick: "week"};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Lastweek Report";
      this.readDashboard(data);
    }
    else if((<HTMLInputElement>document.getElementById('lastmonth')).checked == true){
      var data = {quick: "month"};
      (<HTMLInputElement>document.getElementById('report_period')).innerHTML = "Lastmonth Report";
      this.readDashboard(data);
    }
  }

  checkLastHour(){(<HTMLInputElement>document.getElementById('lasthour')).checked = true;}

  checkLastDay(){(<HTMLInputElement>document.getElementById('lastday')).checked = true;}

  checkLast3Day(){(<HTMLInputElement>document.getElementById('last3day')).checked = true;}

  checkLastWeek(){(<HTMLInputElement>document.getElementById('lastweek')).checked = true;}

  checkLastMonth(){(<HTMLInputElement>document.getElementById('lastmonth')).checked = true;}

}



