import { Injectable } from '@angular/core';
import { HttpService } from './../../services/http.service';
@Injectable()
export class DashboardService {

  constructor(private http: HttpService) { }
  searchQuick(requestData: any){
    return this.http.requestPost('api/Dashboard/Search/quick',requestData);
  }
  searchCustom(requestData: any){
    return this.http.requestPost('api/Dashboard/Search/custom',requestData);
  }

}
export class NewClients{
  public id: number;
  public name: string;
  public surname: string;
  public email: string;
  public phone: number;
  public status: number;
  constructor(id:number, name:string, surname:string, email:string, phone:number, status:number){
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
    this.phone = phone;
    this.status = status;
  }
}
export class NewOrders{
  public id: number;
  public transaction: string;
  public pck_type: string;
  public pck_detail: string;
  public price: number;
  public status: string;
  constructor(id:number, transaction:string, pck_type:string, pck_detail:string, price:number, status:string){
    this.id = id;
    this.transaction = transaction;
    this.pck_type = pck_type;
    this.pck_detail = pck_detail;
    this.price = price;
    this.status = status;
  }
}
