import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class InvoicesService {

  constructor(private http:HttpService) { }

AllInvoices(){
    return this.http.requestPost('api/invoice-bill-all', null);
  }

  InvoicesSend(model:any){
    return this.http.requestPost('api/invoice/send',model);
  }

  Invoicessuspend(model:any){
    return this.http.requestPost('api/invoice/suspend',model);
  }

   Invoicesedit(model:any){
    return this.http.requestPost('api/invoice/edit',model);
  }

  InvoicessDetail(model:any){
    return this.http.requestPost('api/invoice-bill',model);
  }

}
