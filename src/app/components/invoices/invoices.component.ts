import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { InvoicesService } from './invoices.service';

declare let $;
@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss'],
  providers: [InvoicesService]
})
export class InvoicesNavComponent implements OnInit {
 
  formFilterInvoice: FormGroup;
  formInvoiceEdit: FormGroup;
  formInvoiceSend: FormGroup;
  InvoFromConstructor: FormGroup;
  UrlCurrentState: any = [];
  public dataInvoice: any = [];

  message: any;
 invoiceID: any;
 sendinvoice_no:any;
 sendemail:any;
 sendclient_id:any;
 sendpackageId:any;

 editcust_id:any;
 editpck_id = 0;
 editcart_id:any;

 public dataInvoiceDetai: any = [];



  constructor(builder: FormBuilder, private service: InvoicesService) {
    this.formFilterInvoice = builder.group({
      client_id: [''],
      email: [''],
      invoice_id: [''],
      invoice_status: ['']
    });

    this.formInvoiceEdit = builder.group({
      invoice_edit_Suspend: [''],
      invoice_edit_Status: ['ac'],
    });

    this.formInvoiceSend = builder.group({
      invoice_send_Email: ['']
    });

    this.InvoFromConstructor = builder.group({
      client_id: [''],
      email: [''],
      invoice_id: [''],
      package_status: ['']
    });



    this.Initialize();
  }

  ngOnInit() {
    this.InvoicesAll();
  }

  Detail(invoice_no:any,cust_id:any,packageId:any){

    let data = {
      invoice_no:invoice_no,
      cust_id:cust_id,
      packageId:packageId.substring(3)
    }

    this.service.InvoicessDetail(data).subscribe((res:any) => {
        this.dataInvoiceDetai = res.data;
        console.log(res);

        $("#InvoiceDetail").modal('toggle');
     });
  }

  InvoicesAll() {
    this.service.AllInvoices().subscribe(res => {
      this.dataInvoice = res;
    });
  }

  searchInvoice() {
    let client_id = this.formFilterInvoice.controls.client_id.value;
    let email = this.formFilterInvoice.controls.email.value;
    let invoice_id = this.formFilterInvoice.controls.invoice_id.value;
    let invoice_status = this.formFilterInvoice.controls.invoice_status.value;
    if (client_id != '' || email != '' || invoice_id != '' || invoice_status != '') {

      if (client_id != '') {
        this.dataInvoice.data = this.dataInvoice.data.filter((f) => {
          return (f.cust_id == client_id)
        })
      }
      if (email != '') {
        this.dataInvoice.data = this.dataInvoice.data.filter((f) => {
          return (f.email == email)
        })
      }
      if (invoice_id != '') {
        this.dataInvoice.data = this.dataInvoice.data.filter((f) => {
          return (f.invoice_no == invoice_id)
        })
      }
      if (invoice_status != '' && invoice_status != 'All') {
        this.dataInvoice.data = this.dataInvoice.data.filter((f) => {
          return (f.invoice_status == invoice_status)
        })
      }

    } else {
      this.InvoicesAll();
    }

  }

  cleaeInvoice() {
    this.formFilterInvoice.controls['client_id'].setValue('');
    this.formFilterInvoice.controls['email'].setValue('');
    this.formFilterInvoice.controls['invoice_id'].setValue('');
    this.formFilterInvoice.controls['invoice_status'].setValue('');
    this.searchInvoice();
  }



  Initialize() {
    this.UrlCurrentState = 'Invoice';
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  watch_nav(Url: string) {
    this.UrlCurrentState = Url;
    $('#nav_list').toggleClass('active');
    $('.pushmenu-left').toggleClass('pushmenu-open');
    $("#nav_list").hasClass('active')
      ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
      : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
  }

  closeEdit() {
    $('#EditInvoiceDialog').modal('toggle');
  }

EditConfrim(){
  let data = { 
      cust_id:this.editcust_id,
      pck_id:this.editpck_id,
      cart_id:this.editcart_id,
      status:this.formInvoiceEdit.controls.invoice_edit_Status.value
  }
    
   this.service.Invoicesedit(data).subscribe((res:any) => {
          this.message = res.data;
      });


}

edit(cust_id: any,pck_id: any,cart_id: any) {
    this.editcust_id = cust_id;
    this.editpck_id = pck_id.substring(3);
    this.editcart_id = cart_id;
    $('#EditInvoiceDialog').modal('toggle');
}

  closeSend() {
    $('#SendInvoiceDialog').modal('toggle');
  }
  send(invoice_no: any,cust_id:any,package_no:any) {
    this.sendinvoice_no = invoice_no;
    this.sendclient_id = cust_id;
    this.sendpackageId = package_no.substring(3);
    $('#SendInvoiceDialog').modal('toggle');
  }

  suspend(invoice_no: any,package_no:any){
    let data = {
      InvoiceId:invoice_no,
      PackageId:package_no.substring(3)
    }
        this.service.Invoicessuspend(data).subscribe((res:any) => {
           this.message = res.data;
      });

  }
sendInvoiceEmail(){
  this.sendemail =  this.formInvoiceSend.controls.invoice_send_Email.value;
    var pattern = "[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$";
    if (this.sendemail.match(pattern) == null) {
      this.message = 'EmailFormate NOT True';
    } else {
    $('#SendInvoiceDialog').modal('toggle');
      let data = {
        invoice_no:this.sendinvoice_no,
        email:this.sendemail,
        cust_id:this.sendclient_id,
        packageId:this.sendpackageId
      }
       this.service.InvoicesSend(data).subscribe((res:any) => {
           console.log(res == null)
           if(res != null){
            this.message = 'Successful';
           }else{
            this.message = 'Unsuccessful';
           }
      });
    }
}

}
