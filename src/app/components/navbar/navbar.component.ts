import { SignalRUserOnlineService } from './../../services/SignalR.UserOnine.service';
import { UrlConfig } from './../../configs/url.config';
import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { AuthenticationService } from '../../services/authentication.service';
import { VerifyService } from '../../services/verify.service';
declare let $;

@Component({
  selector: 'navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  Url = UrlConfig;
  public role: any = '';
  public name: any = '';
  constructor(private router: Router, private authenticated: AuthenticationService, private verifyService: VerifyService, private service: SignalRUserOnlineService) {
  }

  ngOnInit() {
    this.role = localStorage.getItem('Role');
    this.name = localStorage.getItem('Username');
  }

  goHome(){
    this.router.navigate(['/', UrlConfig.Clients]);
  }

  logout() {    
    //this.service.onDisconnection();
    var pathname = location.pathname.trim();
    if (pathname == '/portals-chat') {
      if (this.verifyService.getActiveChats == 0) {
        this.router.navigate(['/', UrlConfig.Signin]);
        this.authenticated.destroyAuthenticatedByKey('Role');
        this.authenticated.destroyAuthenticatedByKey('Username');
        this.authenticated.destroyAuthenticatedByKey('');
      }
    } else {
      this.router.navigate(['/', UrlConfig.Signin]);
      this.authenticated.destroyAuthenticatedByKey('Role');
      this.authenticated.destroyAuthenticatedByKey('Username');
      this.authenticated.destroyAuthenticatedByKey('');
    }
  }

  public removeInClass() {
    $('.navbar-collapse').removeClass('in');
  }

}