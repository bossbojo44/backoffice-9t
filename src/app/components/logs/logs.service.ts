import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class LogsService {

  constructor(private http: HttpService) { }

   PostLogActionAll() {
    return this.http.requestPost('api/LogAction-all', null);
  }
  PostLogActionFilters(model: any) {
    return this.http.requestPost('api/LogAction-filters',(model));
  } 

}
