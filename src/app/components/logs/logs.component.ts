import { Component, OnInit } from '@angular/core';
import { LogsService } from './logs.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

declare let $;
@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss'],
  providers: [LogsService]
})
export class LogsComponent implements OnInit {

  public LogsDataAlls: any = [];
  formLog: FormGroup;
  public dataLogs: { seq: number, date: string, type: string, desc: string, descEntry: string, ip: string, 
                    user: string, firstname: string, lastname: string, role: string}[];y
                    

  constructor(builder: FormBuilder,private service: LogsService) {
    this.formLog = builder.group({
     date_from:[''],
     date_to:[''],
     role:[''],
    });
  }

  onBlue(e,date){
    setTimeout(() => {
    this.formLog.controls[date].setValue(e.value);
    },100)
  }

  ngOnInit() {
    $('.form').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
      }
    });

    $('.to').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
      }
    });


    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var xx = month+'/'+day+'/'+year;


    this.formLog.controls['date_from'].setValue(xx);
     this.formLog.controls['date_to'].setValue(xx);
    //this.loadDataLogAction();

  }


public loadDataLogAction(){
  this.service.PostLogActionAll().subscribe(res => {
      this.LogsDataAlls = res;
    });
  
}

 btnLogActionFilters() {

  let data = { date_from: $("#date_fromm").val(),
     date_to:$("#date_too").val(),
     role:this.formLog.controls.role.value

  }

   console.log(data);
    this.service.PostLogActionFilters(data).subscribe(res => {
      this.LogsDataAlls = res;
    });
  }

  form() {
    $('.form').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
        console.log(day)
      }
    });
  }
  to() {
    $('.to').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
        console.log(day)
      }
    });
  }

  exportData(){
    console.log(this.dataLogs);
    let result:string = "Logs\r\nSeq#, Date, Type, Description, Description of Entry, IP Address, User, First name, Last name, Role\r\n";
    
    this.dataLogs.forEach(element => {
        result += (element.seq + ", " + element.date + ", " + element.type + ", " + element.desc + ", "
                  + element.descEntry + ", " + element.ip + ", " + element.user + ", " + element.firstname + ", "
                  + element.lastname + ", " + element.role + "\r\n");
    });
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    element.setAttribute('download', 'Logs.csv');

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

}