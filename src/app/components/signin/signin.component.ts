import { VerifyService } from './../../services/verify.service';


import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SigninService } from './signin.service';

import { Router } from '@angular/router';
import { UrlConfig } from '../../configs/url.config';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
  providers: [SigninService]
})
export class SigninComponent implements OnInit {
  formsignin: FormGroup;
  resData: any;
  message: any;
  errorNumber: any;

  constructor(builder: FormBuilder, private service: SigninService, private verifyService: VerifyService, private router: Router) {
    this.formsignin = builder.group({
      username: [''],
      password: [''],
      role: ['']
    });

  }

  ngOnInit() {
    this.verifyService.setTempData(null)
  }

  cancel() {
    this.formsignin.controls['username'].setValue('');
    this.formsignin.controls['password'].setValue('');
    this.formsignin.controls['role'].setValue('Admin');
  }

  keyDownFunction(event) {
    if (event.keyCode == 13) {
      this.login();
    }
  }
  login() {
    let model = this.formsignin.value;
    let data = {
      username: this.formsignin.controls.username.value,
      password: this.formsignin.controls.password.value,
      role: this.formsignin.controls.role.value,
    }

    this.service.Postsignin(data).subscribe((res: any) => {
      this.verifyService.setTempData(res.data.TempData)
      this.router.navigate(['/', UrlConfig.Signinpin]);
    },
      err => {
        this.message = err.json().Message;
      });

  }

}