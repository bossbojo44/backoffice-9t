import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class SigninService {

  constructor(private http:HttpService) { }


Postsignin(model:any){
    return this.http.requestPost('api/signin',model);
  }

}
