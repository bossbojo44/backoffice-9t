import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class ClientsService {
  customerId: number = 0;
  constructor(private http: HttpService) { }

  PostNewClient(cust: NewCust) {
    return this.http.requestPost('api/create-client', cust);
  }

  PostClientsAll() {
    return this.http.requestPost('api/clients-all', null);
  }

  PostClientsFilters(model) {
    return this.http.requestPost('api/clients-filters', model);
  }

  UserActionControl(action_model: any) {
    return this.http.requestPost('api/clients-action', action_model);
  }

  public rptGetActivity(cusId: any) {
    return this.http.requestPost('api/clients/reports/activity', cusId);
  }

  public rptGetPayment(cusId: any) {
    return this.http.requestPost('api/clients/reports/payment', cusId);
  }

  public rptGetBackUps(cusId: any) {
    return this.http.requestPost('api/clients/reports/backups', cusId);
  }

  public SetCustomerId(cust_id: number): void {
    this.customerId = cust_id;
  }

  public resetPassword(reset: any) {
    return this.http.requestPost('api/clients-reset', reset);
  }

}

export class NewCust {
  Email: string;
  Password: string;
  Name: string;
  LastName: string;
  PhoneNumber: string;
  AddressPrefix: string;
  City: string;
  StateOrProvince: string;
  Country: string;
  Postcode: string;
  resetPasswordLink: boolean;
  company: string;

  constructor(email: string, password: string, name: string, lastname: string, phone: string, address: string, city: string, state: string, country: string, postcode: string, reset: boolean, company: string) {
    this.Email = email;
    this.Password = password;
    this.Name = name;
    this.LastName = lastname;
    this.PhoneNumber = phone;
    this.AddressPrefix = address;
    this.City = city;
    this.Country = country;
    this.StateOrProvince = state;
    this.Postcode = postcode;
    this.resetPasswordLink = reset;
    this.company = company;
  }
}