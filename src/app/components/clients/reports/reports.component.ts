import { inject } from '@angular/core/testing';
import { ClientsService } from './../clients.service';
import { Component, OnInit, Input, AfterViewInit, DoCheck, OnChanges } from '@angular/core';

@Component({
  selector: 'clients-report',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  inputs: ['cust_id']
})
export class ReportsclientsComponent implements OnInit, OnChanges {
  cust_id: any;
  activitysJ: any = [];
  paymentsJ: any = [];
  backupsJ: any = [];

  constructor(public service: ClientsService) { }

  ngOnInit() {

  }

  ngOnChanges() {
    this.initReports();
  }

  public initReports() {
    
    if (this.service.customerId == 0) return;

    let post_cust = {
      "customerId": this.service.customerId
    };

    this.service.rptGetActivity(post_cust).subscribe((rsa: any) => {
      this.activitysJ = rsa.data;
      //console.log(this.activitysJ);
    });

    this.service.rptGetPayment(post_cust).subscribe((rsp: any) => {
      this.paymentsJ = rsp.data;
      //console.log(this.paymentsJ);
    });

    this.service.rptGetBackUps(post_cust).subscribe((rsp: any) => {
      this.backupsJ = rsp.data;
      //console.log(this.paymentsJ);
    });
  }
}
