import { URLCurrent } from './../../class/URLCurrent';
import { CountriesService } from './countries.service';
import { UsersHub } from './../chats/chats.service';
import { SharedService } from './../../services/shared.service';
import { ClientsService, NewCust } from './clients.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';


declare let $;
@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html',
  styleUrls: ['./clients.component.scss'],
  providers: [ClientsService, CountriesService]
})
export class ClientsComponent implements OnInit, OnDestroy {
  public role: any = '';
  message: any;
  public UrlCurrentState: string = 'Clients';
  public clientsDataAlls: any = [];
  public formClients: FormGroup;
  public formCSV: FormGroup;
  public CustomerId: number;
  public states: any = [];
  public newCust: NewCust;
  public complete: number = 0;
  private EmpId: number = 0;
  private KeyUrl: string = 'clientcurrenturl';

  constructor(private gbl: SharedService, builder: FormBuilder, private service: ClientsService, public Country: CountriesService) {
    this.formClients = builder.group({
      ID: [""],
      Email: [""],
      Name: [""],
      FName: [""],
      Company: [""],
      StatusFilter: [""],
      exactmatch: [false]
    });

    this.formCSV = builder.group({
      CSVFile: [""]
    });

    this.formClients.controls['StatusFilter'].setValue(6);
    this.formClients.controls['exactmatch'].setValue(false);
    this.EmpId = this.gbl.EmpId;

    this.Initialize();
  }

  watch_nav(Url: string) {
    URLCurrent.set(this.KeyUrl, Url);
    this.UrlCurrentState = Url;

    $('#nav_list').toggleClass('active');
    $('.pushmenu-left').toggleClass('pushmenu-open');
    $("#nav_list").hasClass('active')
      ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
      : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
  }

  ngOnDestroy() {
    //URLCurrent.RemoveByKey(this.KeyUrl);
  }

  Initialize() {
    let current = URLCurrent.get(this.KeyUrl);
    this.UrlCurrentState = current || 'Clients';
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });

          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  ngOnInit() {
    this.role = localStorage.getItem('Role');
    $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("active");
      $("#sidebar").toggleClass("disable");
    });
  }

  submitRegis(email: any, password: any, name: any, lastname: any, phone: any, addr: any, city: any, state: any, country: any, postcode: any, reset: any, company:any) {
    var y = 0;
    /*console.log(email.value);
    console.log(password.value);
    console.log(phone.value);
    console.log(postcode.value);*/
    for (let i: number = 1; i < 12; i++) {
      if ((<HTMLInputElement>document.getElementById(i.toString())).value != "") {
        ////////////////////////////////  check email pattern //////////////////////////////////
        if (i == 1) {
          var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          if (pattern.test(email.value)) {
            console.log('email ok');
            y++;
          }
          else
            console.log('email not ok');
        }
        ////////////////////////////////  check password pattern   ////////////////////////////
        else if (i == 2) {
          var pattern = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*_-])[A-z0-9#?!@$%^&*_-]{8,20}$/;
          if (pattern.test(password.value)) {
            console.log('password ok');
            y++;
          }
          else
            console.log('password not ok');
        }
        /////////////////////////////    check phone number ///////////////////////////////
        else if (i == 5) {
          pattern = /^(?:0|\(?\+66\)?\s?|0\s?)[0-9](?:[\.\-\s]?\d\d){4}$/;
          if (pattern.test(phone.value)) {
            console.log('phone number ok')
            y++;
          }
          else
            console.log('phone not ok');
        }
        /////////////////////////////// check postcode ///////////////////////////////
        else if (i == 10) {
          if (/[0-9]{5}/.test(postcode.value)) {
            console.log('postcode ok');
            y++;
          }
          else
            console.log('post not ok');
        }
        else {
          y++;
        }
      }
    }
    console.log(y);

    if (y < 10)
      alert('Please input new client info completely!');

    else {
      this.newCust = new NewCust(email.value, password.value, name.value, lastname.value, phone.value, addr.value, city.value, state.value, country.value, postcode.value, reset.checked, company.value);
      this.service.PostNewClient(this.newCust).subscribe(
        res => {
          //this.loadDataClients();
          alert('Create new client complete');
          this.complete = 0;
          for (let i: number = 1; i <= 10; i++)
            (<HTMLInputElement>document.getElementById(i.toString())).value = "";

          (<HTMLInputElement>document.getElementById('11')).checked = false;

          for (let i: number = 1; i <= 10; i++) {
            (<HTMLInputElement>document.getElementById('valid-icon' + i.toString())).classList.add('invisible');
            (<HTMLInputElement>document.getElementById('invalid-icon' + i.toString())).classList.add('invisible');
          }
        },
        (error: any) => {
          console.log(error.json().Message);
          alert(error.json().Message);
        });
    }
  }

  checkForm(data: any) {
    var valid = "valid-icon" + data.id;
    var invalid = "invalid-icon" + data.id;
    if (data.value === '') {
      (<HTMLInputElement>document.getElementById(valid)).classList.add('invisible');
      (<HTMLInputElement>document.getElementById(invalid)).classList.remove('invisible');
    }
    else {
      (<HTMLInputElement>document.getElementById(valid)).classList.remove('invisible');
      (<HTMLInputElement>document.getElementById(invalid)).classList.add('invisible');

      //////////////////////////////////////////////// is email ///////////////////////////////////////////
      if (data.id == '1') {
        var pattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if (pattern.test(data.value) == false) {
          (<HTMLInputElement>document.getElementById(valid)).classList.add('invisible');
          (<HTMLInputElement>document.getElementById(invalid)).classList.remove('invisible');
        }
      }

      //////////////////////////////////////////// is password ////////////////////////////////
      if (data.id == '2') {
        var pattern = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*_-])[A-z0-9#?!@$%^&*_-]{8,20}$/;
        if (pattern.test(data.value) == false) {
          (<HTMLInputElement>document.getElementById(valid)).classList.add('invisible');
          (<HTMLInputElement>document.getElementById(invalid)).classList.remove('invisible');
        }
      }

      ///////////////////////////////////////////////  generate state /////////////////////////////////
      if (data.id == '8') {
        var country_index = this.Country.Countries.findIndex(m => m == data.value)
        var province = this.Country.Cities[country_index].split("|");
        this.states = province;
      }

      //////////////////////////////////////////////  is phone ////////////////////////////////////
      if (data.id == '5') {
        pattern = /^(?:0|\(?\+66\)?\s?|0\s?)[0-9](?:[\.\-\s]?\d\d){4}$/;
        if (pattern.test(data.value) == false) {
          (<HTMLInputElement>document.getElementById(valid)).classList.add('invisible');
          (<HTMLInputElement>document.getElementById(invalid)).classList.remove('invisible');
        }
      }

      //////////////////////////////////////////// is postcode //////////////////////////////
      if (data.id == '10') {
        if (!/[0-9]{5}/.test(data.value)) {
          (<HTMLInputElement>document.getElementById(valid)).classList.add('invisible');
          (<HTMLInputElement>document.getElementById(invalid)).classList.remove('invisible');
        }
      }
    }
  }

  genPassword(length) {
    var chars = "[#?!@$%^&*_-])abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }
    var pattern = /^(?=.*\d)(?=.*[A-Z])(?=.*[a-z])(?=.*[#?!@$%^&*_-])[A-z0-9#?!@$%^&*_-]{8,20}$/;
    if (pattern.test(pass) == false)
      this.genPassword(10);
    else {
      (<HTMLInputElement>document.getElementById('2')).value = pass;
      (<HTMLInputElement>document.getElementById('valid-icon2')).classList.remove('invisible');
      (<HTMLInputElement>document.getElementById('invalid-icon2')).classList.add('invisible');
    }
  }

  showClients() {
    (<HTMLInputElement>document.getElementById('clients')).classList.remove('invisible');
    (<HTMLInputElement>document.getElementById('addnew')).classList.add('invisible');
    $("#wrapper").toggleClass("active");
    $("#sidebar").addClass("disable");
  }

  showAddnew() {
    (<HTMLInputElement>document.getElementById('clients')).classList.add('invisible');
    (<HTMLInputElement>document.getElementById('addnew')).classList.remove('invisible');
    $("#wrapper").toggleClass("active");
    $("#sidebar").addClass("disable");
  }

  private checkval_isContains(JSONObjects: JSON) {
    let items = {};
    for (let i in JSONObjects) {
      if (!JSONObjects.hasOwnProperty(i)) return;
      let value = JSONObjects[i];
      if (value && value.toString().trim() != '') {
        items[i] = value;
      }
    }
    return items != null ? items : null;
  }

  public onSubmitFilter() {
    if (this.formClients.valid) {
      let f: any = this.checkval_isContains(this.formClients.value);
      if ($.isEmptyObject(f) || (f.StatusFilter == '6' && !f.hasOwnProperty('ID') && !f.hasOwnProperty('Email')
        && !f.hasOwnProperty('Name') && !f.hasOwnProperty('FName') && !f.hasOwnProperty('Company') && !f.hasOwnProperty('Email'))) {
        this.loadDataClients();
      }
      else {
        this.service.PostClientsFilters(f).subscribe(res => {
          this.clientsDataAlls = res;
        });
      }
    }
    else {
      this.loadDataClients();
    }
  }

  public loadDataClients() {
    this.service.PostClientsAll().subscribe(res => {
      this.clientsDataAlls = res;
    });
  }

  public actionClient(cust_id: number, verify: number, block: number, freeze: number, deletes: number, button: any) {

    let match = (button.innerText.toString().length > 0) ? button.innerText.toString().match(/Block|Unblock|Freeze|Unfreeze|Delete|Restore/) : '';
    if (match == null) return;
    let match_valid = match[0].toString();
    let msg = match_valid + ' user ' + cust_id + '?';
    if (confirm(msg)) {
      let current = 0;
      if (match_valid == 'Not Verified') { current = 0; }
      if (match_valid == 'Verified') { current = 1; }
      if (match_valid == 'Block') { current = 2; }
      if (match_valid == 'Freeze') { current = 3; }
      if (match_valid == 'Delete') { current = 4; }

      let EmpId = (parseInt(localStorage.getItem('EmpId')) || 0);

      let rq = {
        "emp_id": (EmpId || 0), //Example data empId in database flag of "Admin" permission
        "cust_id": cust_id,
        "current_state": current,
        "verify": (match_valid == 'Verified') ? 1 : (match_valid == 'Not Verified') ? 0 : verify,
        "block": (match_valid == 'Block') ? 2 : (match_valid == 'Unblock') ? 0 : block,
        "freeze": (match_valid == 'Freeze') ? 3 : (match_valid == 'Unfreeze') ? 0 : freeze,
        "delete": (match_valid == 'Delete') ? 4 : (match_valid == 'Restore') ? 0 : deletes
      };

      this.service.UserActionControl(rq).subscribe((res: any) => {
        if (res.data.data === 1) {
          alert(match_valid + ' user ' + cust_id + ' success.');
          this.loadDataClients();
        }
      });
    }
  }
  public resetPassword(client: any) {
    console.log(client);
    let msg = 'Reset password for user ' + client.cust_id + ' ?';
    if (confirm(msg)) {
      if (client != null) {
        let reset = { "cust_id": client.cust_id, "email": client.email };
        this.service.resetPassword(reset).subscribe((res: any) => {
          //console.log(res);
          if (res.data == "success") {
            alert("Sent request email for reset password complete!");
          }
          else {
            alert(res.data);
          }
        });
      }
    }
  }

  public exportData() {

    let result: string = "List of clients\r\nID, Email, Name, Family, Company, Status, PackageNo, type, Start Date, End Date\r\n";

    this.clientsDataAlls.data.forEach(element => {
      result += (element.cust_id + ", " + element.email + ", " + element.firstname + ", " + element.familyName + ", "
        + element.company + ", " + element.status + ", " + element.pck_id + ", " + element.pck_type_name + ", "
        + element.pck_start_dt + ", " + element.pck_end_dt + ", " + "\r\n");
    });
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    element.setAttribute('download', 'clients.csv');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  public OpenDlg(_customerId: number) {
    this.service.SetCustomerId(_customerId);
    this.CustomerId = _customerId;
  }
}
