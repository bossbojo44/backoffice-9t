import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AddnewusersService } from './addnewusers.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';

declare let $;
@Component({
  selector: 'app-addnewusers',
  templateUrl: './addnewusers.component.html',
  styleUrls: ['./addnewusers.component.scss'],
  providers: [AddnewusersService]
})
export class AddnewusersComponent implements OnInit {
  disabled: boolean;
  maxLength: any;
  value: any;
  message: any;

  role: any = '';

  @Output('status') status = new EventEmitter();
  formAddUser: FormGroup;
  PositionAll: any = [];
  constructor(builder: FormBuilder, private service: AddnewusersService) {
    this.formAddUser = builder.group({
      emp_username: [''],
      emp_password: [''],
      emp_fristname: [''],
      emp_lastname: [''],
      emp_permission: ['Admin'],
      emp_position: [1],
      emp_status: ['a'],
      emp_email: [''],
      emp_pin: builder.array([
        [''],
        [''],
        [''],
        [''],
        [''],
        ['']
      ])
    });

    //   $(".inputs").keyup(function (e) {
    // 	var key = e.keyCode || e.charCode;
    // 	if(key == 8){
    //   	$(this).prev('.inputs').focus();
    //   }
    //   else{
    //   	if (this.value.length == this.maxLength) {
    //       var $next = $(this).next('.inputs');
    //       if ($next.length)
    //         $(this).next('.inputs').focus();
    //       else
    //         $(this).blur();
    //       }
    //   }
    // });

  }

  ngOnInit() {
    this.role = localStorage.getItem('Role');
    this.loadDataPosition();
    this.randomPassword(8);
  }

  onKeyin(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs-' + sumidndex).focus();
      }
    }
    else {
      var sumindex = index + 1;
      $('.inputs-' + sumindex).focus();
    }
  }
  onKeydow(e, index) {
    var key = e.keyCode || e.charCode;
    console.log(key)
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs-' + sumidndex).focus();
      }
    }
  }


  randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }
    this.formAddUser.controls['emp_password'].setValue(pass);

  }


  loadDataPosition() {
    this.service.PostPositionAll().subscribe((ress: any) => {
      this.PositionAll = ress;
    });
  }


  add() {

    var pattern = "[a-zA-Z0-9]+@addlink.com";
    var str = this.formAddUser.controls.emp_email.value;

    if (str.match(pattern) == null) {
      this.message = 'EmailFormate: ...@addlink.com';
    } else {

      //console.log(this.formAddUser.controls.emp_username.value)
      let model = this.formAddUser.value;
      let dataAdduser = {
        emp_username: this.formAddUser.controls.emp_username.value,
        emp_password: this.formAddUser.controls.emp_password.value,
        emp_fristname: this.formAddUser.controls.emp_fristname.value,
        emp_lastname: this.formAddUser.controls.emp_lastname.value,
        emp_permission: this.formAddUser.controls.emp_permission.value,
        emp_position: this.formAddUser.controls.emp_position.value,
        emp_status: this.formAddUser.controls.emp_status.value,
        emp_pin: model.emp_pin.join(''),
        emp_email: this.formAddUser.controls.emp_email.value,
      }

      if (dataAdduser.emp_username == '' || dataAdduser.emp_password == '' || dataAdduser.emp_fristname == '' || dataAdduser.emp_lastname == '' || dataAdduser.emp_pin == '' ||
        dataAdduser.emp_email == '') {
        this.message = 'data not null';
      } else {
        this.disabled = true;
        this.service.PostAddUser(dataAdduser).subscribe(res => {
        this.disabled = false;

         this.formAddUser.controls['emp_username'].setValue('');
         this.formAddUser.controls['emp_password'].setValue('');
         this.formAddUser.controls['emp_fristname'].setValue('');
         this.formAddUser.controls['emp_lastname'].setValue('');
         this.formAddUser.controls['emp_permission'].setValue('Admin');
         this.formAddUser.controls['emp_position'].setValue('1');
         this.formAddUser.controls['emp_status'].setValue('a');
         this.formAddUser.controls['emp_email'].setValue('');

          $('#adduserdialog').modal('toggle');
          this.status.emit();
        });
      }
    }

  }

  close() {
    this.formAddUser.controls['emp_username'].setValue('');
    this.formAddUser.controls['emp_password'].setValue('');
    this.formAddUser.controls['emp_fristname'].setValue('');
    this.formAddUser.controls['emp_lastname'].setValue('');
    this.formAddUser.controls['emp_permission'].setValue('Admin');
    this.formAddUser.controls['emp_position'].setValue('1');
    this.formAddUser.controls['emp_status'].setValue('a');
    this.formAddUser.controls['emp_email'].setValue('');
    $('#adduserdialog').modal('toggle')
  }

  showpass() {
    this.disabled = false;
    this.randomPassword(8);
  }


  get getFormArray() {
    return (<FormArray>this.formAddUser.get('emp_pin')).controls;
  }

}
