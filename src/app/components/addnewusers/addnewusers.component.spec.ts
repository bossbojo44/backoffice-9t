import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddnewusersComponent } from './addnewusers.component';

describe('AddnewusersComponent', () => {
  let component: AddnewusersComponent;
  let fixture: ComponentFixture<AddnewusersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddnewusersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddnewusersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
