import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class AddnewusersService {

  constructor(private http:HttpService) { }

  PostPositionAll(){
    return this.http.requestPost('api/position-all',null);
  }

  PostAddUser(model:any){
    return this.http.requestPost('api/add-user',model);
  }

}
