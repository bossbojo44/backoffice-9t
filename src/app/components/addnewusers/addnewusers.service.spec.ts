import { TestBed, inject } from '@angular/core/testing';

import { AddnewusersService } from './addnewusers.service';

describe('AddnewusersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AddnewusersService]
    });
  });

  it('should ...', inject([AddnewusersService], (service: AddnewusersService) => {
    expect(service).toBeTruthy();
  }));
});
