import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from '../request.service';
import { HttpService } from '../../../services/http.service';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

declare let $;
@Component({
  selector: 'app-upgrade-package-request',
  templateUrl: './upgrade-package-request.component.html',
  styleUrls: ['./upgrade-package-request.component.scss']
})
export class UpgradePackageRequestComponent implements OnInit {
  formFilterRequest: FormGroup;
  Allrequest: any;
  logActiveSave:logRequest[] = [];
  BackUP: any;
  ObjgenerateIn: any;
  pageAll: number;
  Nowpage: number = 1;
  limit: number = 20;
  pagePrint: number[] = [];
  constructor(builder: FormBuilder, private service: RequestService, private http: HttpService) {
    this.onGetallRequest();
    this.formFilterRequest = builder.group({
      client_id: [''],
      email: [''],
      invoice_id: [''],
      package_status: ['']
    });
  }

  ngOnInit() {
    setTimeout(() => { $('#btnPage' + 1).css("backgroundColor", "#fff") }, 500);
  }
  removeItem(id) {
    this.http.requestDelete('api/RemovePack?id=' + id).subscribe((res) => {
      console.log(res);
      this.onGetallRequest();
    });
  }
  onGetallRequest() {
    this.http.requestGet('api/GetallRequest').subscribe((res) => {
      this.Allrequest = res;
      this.Allrequest = this.Allrequest.data;
      this.BackUP = res;
      this.BackUP = this.BackUP.data;
    });
  }
  generate(generatethis) {
    this.ObjgenerateIn = generatethis;
    $('#requestdialog').modal();
  }
  reset(dataEvent) {
    this.onGetallRequest();
  }
  Gofilter() { // use filter on botton 
    console.log(this.formFilterRequest.value);
    
    this.Allrequest = this.BackUP;
    var result: any = [];
    result = this.SearchCID(this.formFilterRequest.value.client_id, this.Allrequest);
    if (result != null) {
      result = this.SearchINID(this.formFilterRequest.value.invoice_id, result)
      if (result != null) {
        result = this.SearchEmail(this.formFilterRequest.value.email, result);
        if (result != null) {
          this.Allrequest = result;
        }
      }
    }
  }
  OnClear() { // clear filter all 
    this.formFilterRequest.value.client_id = null;
    this.formFilterRequest.value.invoice_id = null;
    this.formFilterRequest.value.email = null;
  }
  SearchCID(CID, elm) { // filter for cilnet ID
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.cust_id.toString().match(CID)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  SearchINID(INID, elm: any) { // filter for invoice ID
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.vcc_id.toString().match(INID)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  SearchEmail(MAIL, elm) {// filter for email
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.email.toString().match(MAIL)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  onSaveFileCSV() {
    if (this.Allrequest.length == 0) {
      alert('filter null');
      return;
    }
    let data = this.Allrequest;
    this.logActiveSave.push(new logRequest('Package No', 'Package type','Package status', 'Client ID', 'Email', 'Name', 'Family', 'Company', 'Created', 'Current storage','Current VM','New storage','New VM'));
    for (var i = 0; i < data.length; i++) {
      this.logActiveSave.push(new logRequest(data[i].vcc_id, data[i].pck_type_name, 'Active', data[i].cust_id, data[i].email, data[i].firstname, data[i].lastname, data[i].company_name,'manually',data[i].storage,data[i].vm,data[i].new_storage,data[i].new_vm));
    }
    new Angular2Csv(this.logActiveSave, 'Upgrade package request report');
    this.logActiveSave = [];
  }

}
class logRequest{
  constructor(Package_No:string,Package_type:string,Package_status:string,Client_ID:string,Email:string,Name:string,Family:string,Company:string,Created:string
    ,Current_storage:string,Current_VM:string,New_storage:string,New_VM:string){
    this.Package_No = Package_No;
    this.Package_type = Package_type;
    this.Package_status = Package_status;
    this.Client_ID = Client_ID;
    this.Email = Email;
    this.Name = Name;
    this.Family = Family;
    this.Company = Company;
    this.Created =Created;
    this.Current_storage = Current_storage;
    this.Current_VM = Current_VM;
    this.New_storage = New_storage;
    this.New_VM = New_VM;
  }
  Package_No:string;
  Package_type:string;
  Package_status:string;
  Client_ID:string;
  Email:string;
  Name:string;
  Family:string;
  Company:string;
  Created:string;
  Current_storage:string;
  Current_VM:string;
  New_storage:string;
  New_VM:string;
}
  