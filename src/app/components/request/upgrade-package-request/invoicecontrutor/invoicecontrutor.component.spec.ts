import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvoicecontrutorComponent } from './invoicecontrutor.component';

describe('InvoicecontrutorComponent', () => {
  let component: InvoicecontrutorComponent;
  let fixture: ComponentFixture<InvoicecontrutorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvoicecontrutorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvoicecontrutorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
