import { Component, Input, OnChanges, Output, EventEmitter, OnInit } from '@angular/core';
import { HttpService } from '../../../../services/http.service';
import { FormBuilder , FormGroup} from '@angular/forms';

declare let $;
@Component({
  selector: 'app-invoicecontrutor',
  templateUrl: './invoicecontrutor.component.html',
  styleUrls: ['./invoicecontrutor.component.scss']
})

export class InvoicecontrutorComponent implements OnChanges,OnInit {

  @Input() DetailInvoice;
  @Output('CanCloseInvo') CanCloseInvo = new EventEmitter();
  Info_email:any;
  CanCloseInvoTo:boolean = true;
  HiddencanShow:boolean = true;
  returnResult_sendEmail:any;
  VB:FormGroup;
  constructor(private http:HttpService ,private build:FormBuilder) {
    this.VB = this.build.group({
      vmlicenses:'',
      storage:''
    });
  }
  ngOnInit(){
  }
  ngOnChanges() {
    if(this.DetailInvoice){
      this.VB.value.vmlicenses = this.DetailInvoice.new_vm;
      this.VB.value.storage =  this.DetailInvoice.new_storage;
    }
    //this.DetailInvoice = this.DetailInvoice[0];
    //console.log(this.DetailInvoice);
  }
  goGenerateInvoice(dataGenerate,stor,vm) {
    this.VB.value.vmlicenses =stor;
    this.VB.value.storage = vm;
    this.CanCloseInvoTo = false;
    var obj = new m_generateInvoioce(dataGenerate.pck_id,this.VB.value.vmlicenses,this.VB.value.storage,dataGenerate.premiums_storage);
    this.http.requestPost("api/ganerateInvoice",obj).subscribe((res)=>{
      this.Info_email = res;
      this.Info_email = this.Info_email.data[0];
      this.HiddencanShow = false;
    });
  }
  OnSendEmail(email,obj){
    $('#WaitLoding').fadeIn();
    $('#SavevSend').html('loading...');
    this.http.requestPut("api/SendEmail_ganerateInvoice?email="+email,obj).subscribe((res)=>{
      this.returnResult_sendEmail = res;
      console.log(this.returnResult_sendEmail.data.Result);
      
      if(this.returnResult_sendEmail.data.Result){
        $('#myLoading').modal('toggle');
        this.CanCloseInvoTo = true;
        this.CanCloseInvo.emit(this.CanCloseInvoTo);
        $('#WaitLoding').fadeOut();
        $('#SavevSend').html('Save & Send');
        $('#close_modal').click();
      } //data: true
    });
  }
  clearValue(){
    console.log('clear');
    this.HiddencanShow = true;
  }

}

class m_generateInvoioce{
  constructor(pck_id:number,stor_vb:number,vm_vb:number,premiums_storage:boolean){
    this.pck_id = pck_id;
    this.stor_vb = stor_vb;
    this.vm_vb = vm_vb;
    this.premiums_storage = premiums_storage;
  }
  pck_id:number;
  stor_vb:number;
  vm_vb:number;
  premiums_storage:boolean;
  
}