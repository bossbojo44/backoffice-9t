import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpgradePackageRequestComponent } from './upgrade-package-request.component';

describe('UpgradePackageRequestComponent', () => {
  let component: UpgradePackageRequestComponent;
  let fixture: ComponentFixture<UpgradePackageRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpgradePackageRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpgradePackageRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
