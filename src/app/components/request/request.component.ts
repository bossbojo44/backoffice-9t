import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from './request.service';
import { HttpService } from '../../services/http.service';
declare let $;
@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
 // providers: [RequestService]
})
export class RequestComponent implements OnInit {
  InvoFromConstructor: FormGroup;
  RequestFrom: FormGroup;
  formFilterNewPackageRequest: FormGroup;
  formFilterRequestHistory: FormGroup;
  UrlCurrentState: any = [];
  Allrequest: any;
  ObjgenerateIn:any;
  DetailInvoice;
  TimeNow:Date = new Date();
  DataForGaneraInvoice:any;
  openInvoioce:boolean = false;

  constructor(builder: FormBuilder, private service: RequestService, private http: HttpService) {
  
    this.InvoFromConstructor = builder.group({
      client_id: [''],
      email: [''],
      invoice_id: [''],
      package_status: ['']
    });

    this.Initialize();
  }

  ngOnInit() {

  }
  
  updatePack(updatePackThis){
    console.log(updatePackThis);
    $('#updatedialog').modal();
  }
  setValueUpdate(generatethis,stor,vm,time){
    console.log(this.TimeNow);
    generatethis.new_stor_vb =stor;
    generatethis.new_vm_vb =vm;
    this.ObjgenerateIn = generatethis
  }
  clearValue(){
    this.ObjgenerateIn = '';
    this.openInvoioce = false;
  }
  Initialize() {
    this.UrlCurrentState = 'UpgradeRequest'; //Upgraderequests
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  watch_nav(Url: string) {
    this.UrlCurrentState = Url;
    $('#nav_list').toggleClass('active');
    $('.pushmenu-left').toggleClass('pushmenu-open');
    $("#nav_list").hasClass('active')
      ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
      : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });
  }
}
class m_generateInvoioce{
  constructor(startDate:Date,pck_id:number,stor_vb:number,vm_vb:number,premiums_storage:boolean){
    this.startDate = startDate;
    this.pck_id = pck_id;
    this.stor_vb = stor_vb;
    this.vm_vb = vm_vb;
    this.premiums_storage = premiums_storage;
  }
  startDate:Date;
  pck_id:number;
  stor_vb:number;
  vm_vb:number;
  premiums_storage:boolean;
  
}
