import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestHistoryComponent } from './request-history.component';

describe('RequestHistoryComponent', () => {
  let component: RequestHistoryComponent;
  let fixture: ComponentFixture<RequestHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component.ngOnInit).toBeTruthy();
  });
   it('should clearData', () => {
    // this.component.clearData();
     expect(component.clearData).toEqual(this.requestHistory=['']);
 });
});
