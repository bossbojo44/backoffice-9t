import { Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from 'app/components/request/request.service';
import { HttpService } from 'app/services/http.service';
import { UrlConfig } from '../../../configs/url.config';
declare let $;
@Component({
  selector: 'app-request-history',
  templateUrl: './request-history.component.html',
  styleUrls: ['../request.component.scss']
})
export class RequestHistoryComponent implements OnInit {
  formFilterRequestHistory: FormGroup;
  requestHistory: any = [];
  Url = UrlConfig;
  constructor(builder: FormBuilder, private service: RequestService, private http: HttpService) {
    this.formFilterRequestHistory = builder.group({
      from: [''],
      to: [''],
      client_id: [''],
      package_type: ['']
    });
    this.requestHistorys();
  }

  ngOnInit() {
  }
  requestHistorys() {
    return this.http.requestGet('api/getRequestHistory').subscribe((res) => {
      this.requestHistory = res;
      this.requestHistory = this.requestHistory.data;
      console.log(this.requestHistory)
    });

  }
  clearData() {
    this.requestHistory = [''];
    this.requestHistorys();
    //  return this.requestHistory = [''];
  }

  searchRequestHistory() {
    let client_id = this.formFilterRequestHistory.controls.client_id.value;
    //let email = this.formFilterRequestHistory.controls.email.value;
    // let package_type = this.formFilterRequestHistory.controls.package_type.value;
    if (client_id != null) {
      this.requestHistory = this.requestHistory.filter((f) => {
        return (f.cust_id == client_id);
      });
    }
    //  if (package_type != '' ) {
    //    this.requestNewPackage= this.requestNewPackage.filter((f) => {
    //   return (f.package_type== package_type)
    //    })
    //  }
    //console.log(package_type);
    else {
      this.requestHistorys();
    }
  }

  removeItem(id) {
    if (confirm("Are you sure you want to delete?")) {
       this.http.requestDelete('api/deleteRequestHistory?id=' + id).subscribe(() => {
        this.requestHistorys();
      });
      return alert('Remove Success!');
    } else {

    }
  }

onclick(id){

  this.service.onSave(id);
  console.log(id);
}
}
