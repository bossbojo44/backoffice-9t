import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from '../request.service';
import { HttpService } from '../../../services/http.service';
import { Http } from '@angular/http';
import { Angular2Csv } from 'angular2-csv/Angular2-csv';

declare let $;
@Component({
  selector: 'app-upgrade-request',
  templateUrl: './upgrade-request.component.html',
  styleUrls: ['./upgrade-request.component.scss']
})
export class UpgradeRequestComponent implements OnInit {
  formFilterUpgrade: FormGroup;
  Allrequest: any;
  BackUP:any;
  ObjgenerateInUP: any;
  obj: any;
  pageAll: number;
  Nowpage: number = 1;
  limit: number = 20;
  backupdata: any;
  pagePrint: number[] = [];
  logActiveSave:any;
  constructor(builder: FormBuilder, private service: RequestService, private http: HttpService) {
    this.onGetallRequest();
    this.formFilterUpgrade = builder.group({
      client_id: [''],
      email: [''],
      invoice_id: [''],
      package_status: ['']
    });
  }

  ngOnInit() {
    setTimeout(()=>{$('#btnPage'+1).css("backgroundColor","#fff")},500);
  }
  removeItem(id) {
    this.http.requestDelete('api/RemovePack?id=' + id).subscribe((res) => {
      console.log(res);
      this.onGetallRequest();
    });
  }
  onGetallRequest() {
    this.http.requestGet('api/GetallRequestUP').subscribe((res) => {
      this.Allrequest = res;
      this.Allrequest = this.Allrequest.data;
      this.BackUP = res;
      this.BackUP = this.BackUP.data;
    });
  }
  updatePackage(request) {
    this.ObjgenerateInUP = request;
    this.obj = {
      pck_id: request.pck_id,
      new_storage: request.new_storage,
      new_vm: request.new_vm,
    }
    $('#updatedialog').modal();
  }
  GoToUpgrade() {
    $('#WaitLoding').fadeIn();
    this.http.requestPost("api/UpdatePackageRequest", this.obj).subscribe((res) => {
      var result: any = res;
      if (result.data) {
        this.onGetallRequest();
        $('#WaitLoding').fadeOut();
        $('#updatedialog').modal('toggle');
      }
    });
  }
  ActivePage(page){
    for (var i = 1; i <= this.pageAll; i++) {
     if(i == page){
        $('#btnPage'+i).css("backgroundColor","#fff")
     }else{
        $('#btnPage'+i).css("backgroundColor","#ddd")
     }
    }
  }
  Gofilter() {
    this.Allrequest = this.BackUP;
    var result: any = [];
    result = this.SearchCID(this.formFilterUpgrade.value.client_id, this.Allrequest);
    if (result != null) {
      result = this.SearchINID(this.formFilterUpgrade.value.invoice_id, result)
      if (result != null) {
        result = this.SearchEmail(this.formFilterUpgrade.value.email, result);
        if (result != null) {
          this.Allrequest = result;
        }
      }
    }
  }
  OnClear() {
    this.formFilterUpgrade.value.client_id = null;
    this.formFilterUpgrade.value.invoice_id = null;
    this.formFilterUpgrade.value.email = null;
  }
  SearchCID(CID, elm) {
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.cust_id.toString().match(CID)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  SearchINID(INID, elm: any) {
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.vcc_id.toString().match(INID)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  SearchEmail(MAIL, elm) {
    let i = 0;
    var result: any = [];
    elm.forEach(element => {
      if (element.email.toString().match(MAIL)) {
        result[i] = element; i++;
      }
    });
    return result;
  }
  onSaveFileCSV() {
    if (this.Allrequest.length == 0) {
      alert('filter null');
      return;
    }
    let data = this.Allrequest;
    this.logActiveSave.push(new logRequest('Package No', 'Package type','Package status', 'Client ID', 'Email', 'Name', 'Family', 'Company', 'Created', 'Current storage','Current VM','New storage','New VM','New Invoice ID'));
    for (var i = 0; i < data.length; i++) {
      this.logActiveSave.push(new logRequest(data[i].vcc_id, data[i].pck_type_name, 'Resize', data[i].cust_id, data[i].email, data[i].firstname
      ,data[i].lastname, data[i].company_name,'manually',data[i].storage,data[i].vm,data[i].new_storage,data[i].new_vm,data[i].pck_id_ref));
    }
    new Angular2Csv(this.logActiveSave, 'Upgrade package report');
    this.logActiveSave = [];
  }

}
class logRequest{
  constructor(Package_No:string,Package_type:string,Package_status:string,Client_ID:string,Email:string,Name:string,Family:string,Company:string,Created:string
    ,Current_storage:string,Current_VM:string,New_storage:string,New_VM:string,New_Invoice_ID:string){
    this.Package_No = Package_No;
    this.Package_type = Package_type;
    this.Package_status = Package_status;
    this.Client_ID = Client_ID;
    this.Email = Email;
    this.Name = Name;
    this.Family = Family;
    this.Company = Company;
    this.Created =Created;
    this.Current_storage = Current_storage;
    this.Current_VM = Current_VM;
    this.New_storage = New_storage;
    this.New_VM = New_VM;
    this.New_Invoice_ID = New_Invoice_ID;
  }
  Package_No:string;
  Package_type:string;
  Package_status:string;
  Client_ID:string;
  Email:string;
  Name:string;
  Family:string;
  Company:string;
  Created:string;
  Current_storage:string;
  Current_VM:string;
  New_storage:string;
  New_VM:string;
  New_Invoice_ID:string;
}