import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestNewpackageComponent } from './request-newpackage.component';

describe('RequestNewpackageComponent', () => {
  let component: RequestNewpackageComponent;
  let fixture: ComponentFixture<RequestNewpackageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestNewpackageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestNewpackageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
