import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { RequestService } from 'app/components/request/request.service';
import { HttpService } from 'app/services/http.service';
import { UrlConfig } from '../../../configs/url.config';
@Component({
  selector: 'app-request-newpackage',
  templateUrl: './request-newpackage.component.html',
  styleUrls: ['../request.component.scss']
})
export class RequestNewpackageComponent implements OnInit {
  formFilterNewPackageRequest: FormGroup;
  requestNewPackage: any = [];
  Url = UrlConfig;
  constructor(builder: FormBuilder, private service: RequestService, private http: HttpService) {
    this.formFilterNewPackageRequest = builder.group({
      client_id: [''],
      email: [''],
      package_type: ['']
    });
    this.requestnewpackage();
  }

  ngOnInit() {
  }
  requestnewpackage() {
    return this.http.requestGet('api/getRequestPackage').subscribe((res) => {
      this.requestNewPackage = res;
      this.requestNewPackage = this.requestNewPackage.data;
    });
  }
  clearData() {
    this.requestNewPackage = [''];
    this.requestnewpackage()
  }

  searchRequest() {
    let client_id = this.formFilterNewPackageRequest.controls.client_id.value;
    let email = this.formFilterNewPackageRequest.controls.email.value;
    //  let package_type = this.formFilterNewPackageRequest.controls.package_type.value;
    console.log(client_id);
    switch (client_id != null || email != null) {
      case client_id != '':
        this.requestNewPackage = this.requestNewPackage.filter((f) => {
          return (f.cust_id == client_id);
        });
        break;
      case email != '':
        this.requestNewPackage = this.requestNewPackage.filter((f) => {
          return (f.email == email);
        });
        break;
      default:
        this.requestnewpackage();
    }
  }
  removeItem(id) {
    if (confirm("Are you sure you want to delete?")) {
      this.http.requestDelete('api/deleteRequestPackage?id=' + id).subscribe(() => {
        this.requestnewpackage();
      });
      return alert('Remove Success!');
    } else {

    }
  }
  onclick(id){
    
      this.service.onSave(id);
      console.log(id);
    }
}
