import { Pipe, PipeTransform } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { PaginationService } from '../../services/pagination.service';
@Pipe({
    name: 'pagination',
    pure: false
})
export class PaginationPipe implements PipeTransform {
    constructor(private service: PaginationService, private activatedRoute: ActivatedRoute) { }
    transform(items: any[], limit: number = 14) {
        if (!items) return [];
        this.service.setLimit(limit);
        let page = this.activatedRoute.snapshot.queryParams['page'] || 1;
        let endPage = Math.ceil(items.length / limit);
        let startPage = (page <= endPage) ? page : endPage;
        return this.service.processPagination(items, startPage, limit);
    }

}