import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnChanges, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { PaginationService } from '../../services/pagination.service';

declare let $;
@Component({
    selector: 'pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnChanges, OnDestroy {
    constructor(private router: Router, private activatedRoute: ActivatedRoute, private service: PaginationService) {
        // process event of route
        this._subscription = this.router.events.subscribe(r => {
            // find some query param
            this.activatedRoute.queryParams.forEach(p => {
                this.page = parseInt((p['page'] || 1));
            });
        });
    }

    private _subscription: Subscription;

    @Input('limit') limit: number;
    @Input('items') items: any[];
    @Input('pageLoading') loading: boolean;
    @Output('onPageChange') pageChange = new EventEmitter();

    page: number = 1;
    options: PageOption[] = [];
    optionLength: number = 0;
    disabled = {
        next: false,
        prev: false
    };

    listpage: any = []

    // on changed
    ngOnChanges(): void {
        var urlParams = new URLSearchParams(window.location.search);
        if (urlParams.get('page') != null) {
            this.page = parseInt(urlParams.get('page'));
        }

        if (!this.items || this.options.length > 0) return;
        if (this.items.length == 0) return;
        this.limit = this.service.getLimit || this.limit;
        this.listpage = [];
        this.optionLength = Math.ceil(this.items.length / this.limit);

        if (this.optionLength >= 6) {
            if (this.page + 4 >= this.optionLength) {
                this.listpage = [];
                for (let i = this.page - 4; i <= this.optionLength; i++) {
                    if (i <= this.optionLength) {
                        this.listpage.push({ value: i, text: `${i}` });
                    }
                }
            } else {
                this.listpage = [];
                for (let i = this.page; i <= this.page + 4; i++) {
                    if (i <= this.optionLength) {
                        this.listpage.push({ value: i, text: `${i}` });
                    }
                }
            }

        } else {
            for (let i = this.page; i <= this.optionLength; i++) {
                if (i <= this.optionLength) {
                    this.listpage.push({ value: i, text: `${i}` });
                }
            }
        }







    }


    ngOnNewpage(page) {
        if (this.optionLength >= 6 ) {
            if (page + 4 >= this.optionLength) {
                this.listpage = [];
                for (let i = page - 4; i <= this.optionLength; i++) {
                    if (i <= this.optionLength) {
                        this.listpage.push({ value: i, text: `${i}` });
                    }
                }
            } else {
                this.listpage = [];
                for (let i = page; i <= page + 4; i++) {
                    if (i <= this.optionLength) {
                        this.listpage.push({ value: i, text: `${i}` });
                    }
                }
            }
        } else {
            for (let i = this.page; i <= this.optionLength; i++) {
                if (i <= this.optionLength) {
                    this.listpage.push({ value: i, text: `${i}` });
                }
            }
        }







    }

    // clear cache
    ngOnDestroy() {
        this._subscription.unsubscribe();
    }
    onNextPage() {
        if (this.onCheckNextPage) return;
        this.redirectToPate(++this.page);
        this.ngOnNewpage(++this.page);
    }

    // button on previous page
    onPreviousPage() {
        if (this.onCheckPrevPage) return;
        this.redirectToPate(--this.page);
        this.ngOnNewpage(--this.page);
    }

    onPageOne() {
        this.redirectToPate(1);
        this.ngOnNewpage(1);

    }

    onPageEnd() {
        this.redirectToPate(this.optionLength);
        this.ngOnNewpage(this.optionLength);

    }

    // redurect page
    private redirectToPate(page): void {
        // on process page event
        this.processPageEvent(page);
        // redirect page
        this.router.navigate([], { queryParams: { page } });
    }

    // check next page
    private get onCheckNextPage() {
        return this.page > (this.optionLength - 1);
    }

    // check previous page
    private get onCheckPrevPage() {
        return this.page <= 1;
    }

    // on process page event
    private processPageEvent(page) {
        // response page
        this.pageChange.emit(page);
    }
}

export class PageOption {
    value: number;
    text: string;
}