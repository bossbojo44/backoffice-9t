import {Directive,ElementRef,Input,OnInit} from '@angular/core';
declare var google:any;
declare var googleLoaded:any;
declare let $;
@Directive({
  selector: '[GoogleChart]'
})
export class GoogleChartComponent implements OnInit {
  public _element:any;
  @Input('chartType') public chartType:string;
  @Input('chartOptions') public chartOptions: Object;
  @Input('chartData') public chartData: Object;
  constructor(public element: ElementRef) {
    //this._element = this.element.nativeElement;
    this._element = "pie_chart";
  }
  ngOnInit() {
   /* setTimeout(() =>{
      google.charts.load('current', {'packages':['corechart']});
        setTimeout(() =>{
          this.drawGraph(this.chartOptions,this.chartType,this.chartData,this._element)
          console.log(this.chartOptions);
          console.log(this.chartType);
          console.log(this.chartData);
          console.log(this._element);
        },1000);
      },1000
    );*/
  }
  drawGraph (chartOptions,chartType,chartData,ele, success:string, inProgress:string, notSuccess:string, unpaid:string, unpaidActive:string, overdue:string, overdueActive:string) {
    var dataTable = new google.visualization.DataTable();
    google.charts.setOnLoadCallback(drawChart);
    var selectRow;
    var pie_ChartOptions0  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 0: {offset: 0.3}},
      };
    var pie_ChartOptions1  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 1: {offset: 0.3}},
      };
      var pie_ChartOptions2  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 2: {offset: 0.3}},
      };
      var pie_ChartOptions3  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 3: {offset: 0.3}},
      };
      var pie_ChartOptions4  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 4: {offset: 0.3}},
      };
      var pie_ChartOptions5  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 5: {offset: 0.3}},
      };
      var pie_ChartOptions6  = {
        legend: { maxLines: 7 },
        //title: 'SALES',
        width: 600,
        height: 400,
        //colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        colors: ['#629C44', '#FFA834', '#FF3823', '#ff8000', '#ffd966', '#0099ff', '#33d6ff'],
        chartArea:{width:'90%',height:'80%'},
        sliceVisibilityThreshold: 0,
        is3D: true,
        tooltip: { trigger: 'selection' },
        slices: { 6: {offset: 0.3}},
      };
   /*
    console.log(success);
    console.log(notSuccess);
    console.log(unpaid);
    console.log(unpaidActive);
    console.log(overdue);
    console.log(overdueActive);*/



    var Success = "Suceessful\r\n- "+chartData[1][1]+" orders\r\n"+"- Value: $"+success;
    var InProgress = "In progress\r\n- "+chartData[2][1]+" orders\r\n"+"- Value: $"+inProgress;
    var NotSuccess = "Not Suceessful\r\n- "+chartData[3][1]+" orders\r\n"+"- Value: $"+notSuccess;
    var Unpaid = "Unpaid\r\n- "+chartData[4][1]+" orders\r\n"+"- Value: $"+unpaid;
    var UnpaidActive = "Unpaid (active)\r\n- "+chartData[5][1]+" orders\r\n"+"- Value: $"+unpaidActive;
    var Overdue = "Overdue\r\n- "+chartData[6][1]+" orders\r\n"+"- Value: $"+overdue;
    var OverdueActive = "Overdue (active)\r\n- "+chartData[7][1]+" orders\r\n"+"- Value: $"+overdueActive;

    dataTable.addColumn('string', 'Orders');
    dataTable.addColumn('number', 'count');
    dataTable.addColumn({type:'string', role:'tooltip'});
    dataTable.addRows([
      ['Successful',  chartData[1][1], Success],
      ['In progress',  chartData[2][1], InProgress],
      ['Not Successful', chartData[3][1], NotSuccess],
      ['Unpaid', chartData[4][1], Unpaid],
      ['Unpaid (active)', chartData[5][1], UnpaidActive],
      ['Overdue', chartData[6][1], Overdue],
      ['Overdue (active)', chartData[7][1], OverdueActive]
    ]);

    function drawChart() {
      /*var wrapper;
      wrapper = new google.visualization.ChartWrapper({
        chartType: chartType,
        dataTable:dataTable ,
        options:chartOptions || {},
        //containerId: ele.id
        containerId: ele
      });
      wrapper.draw();*/
      var chart = new google.visualization.PieChart(document.getElementById('pie_chart'));

      chart.draw(dataTable, chartOptions);
      
      google.visualization.events.addListener(chart, 'select', selectHandler);
      google.visualization.events.addListener(chart, 'onmouseover', function(entry) {
        chart.setSelection([{row: entry.row}]);
        
        selectRow = [{row: entry.row}];
        $('#pie_chart').css('cursor','pointer')
        //console.log("hover => "+selectRow[0].row);

       /* selectRow = entry.row;
        console.log(selectRow);     
        var selection = chart.getSelection();
        console.log(selection);*/
      });
     /* google.visualization.events.addListener(chart, 'onmouseout', function(entry) {
          chart.setSelection([]);
      }); */
      function selectHandler() {
       //var selection = chart.getSelection();

        /*if(chartData[2][1] == 0 && selectRow[0].row == 1){}
        else{
        chartOptions.slices[dataTable.getSortedRows([{column: 1, desc: true}])[selectRow[0].row]] = {offset: 0.3};
        console.log("click => "+selectRow[0].row);
        //console.log("selection => "+selection);*/
        
        if(selectRow[0].row == 0)
          chart.draw(dataTable, pie_ChartOptions0);
        else if(selectRow[0].row == 1)
          chart.draw(dataTable, pie_ChartOptions1);
        else if(selectRow[0].row == 2)
          chart.draw(dataTable, pie_ChartOptions2);
        else if(selectRow[0].row == 3)
          chart.draw(dataTable, pie_ChartOptions3);
        else if(selectRow[0].row == 4)
          chart.draw(dataTable, pie_ChartOptions4);
        else if(selectRow[0].row == 5)
          chart.draw(dataTable, pie_ChartOptions5);
        else if(selectRow[0].row == 6)
          chart.draw(dataTable, pie_ChartOptions6);
      }
    }
  }
}
