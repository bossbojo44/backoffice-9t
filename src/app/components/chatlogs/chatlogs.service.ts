import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class ChatlogsService {

  constructor(private http: HttpService) { }
PostChatLogFilters(model: any) {
    return this.http.requestPost('api/filter-ChatLog',(model));
  } 
}
