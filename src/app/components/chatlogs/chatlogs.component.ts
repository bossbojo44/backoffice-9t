import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,Validators} from '@angular/forms';
import { ChatlogsService } from './chatlogs.service';
declare let $;
@Component({
  selector: 'app-chatlogs',
  templateUrl: './chatlogs.component.html',
  styleUrls: ['./chatlogs.component.scss'], 
  providers: [ChatlogsService]
})
export class ChatlogsComponent implements OnInit {

   public ChatLogsDataAlls: any = [];
  formChatlogs: FormGroup;
  constructor(builder: FormBuilder,private service: ChatlogsService) {
    
     this.formChatlogs = builder.group({
     From:[''],
     To:[''],
     Operator:[''],
     ClientID:[''],
    });
  }

  onBlue(e,date){
    setTimeout(() => {
    this.formChatlogs.controls[date].setValue(e.value);
    },100)
  }

  ngOnInit() {
    $('.form').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
      }
    });

    $('.to').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
      }
    });


    var d = new Date(),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    var xx = month+'/'+day+'/'+year;


    this.formChatlogs.controls['From'].setValue(xx);
     this.formChatlogs.controls['To'].setValue(xx);
     
    //this.loadDataLogAction();

  }

PostChatLogFilters(){
  this.service.PostChatLogFilters(this.formChatlogs.value).subscribe(res => {
      this.ChatLogsDataAlls = res;
      console.log(res)
    });
}



  form() {
    $('.form').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
        console.log(day)
      }
    });
  }
  to() {
    $('.to').datepicker({
      dateFormat: "mm/dd/yy", onSelect: function (dateText) {
        var d = new Date(dateText);
        var day = d.getDate;
        console.log(day)
      }
    });
  }
}