import { TestBed, inject } from '@angular/core/testing';

import { ChatlogsService } from './chatlogs.service';

describe('ChatlogsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatlogsService]
    });
  });

  it('should ...', inject([ChatlogsService], (service: ChatlogsService) => {
    expect(service).toBeTruthy();
  }));
});
