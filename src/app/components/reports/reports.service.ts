import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class ReportsService {

  constructor(private http: HttpService) { }
  PostPackagesAll() {
    return this.http.requestPost('api/package-all', null);
  }

  PostReportFilters(model: any) {
    return this.http.requestPost('api/report-filters', model);
  }

  PostReportDetails(TenantName: string) {
    let pos = {
      "tenant_name": TenantName
    };
    return this.http.requestPost('api/report-job-details', pos);
  }

  GetveeamAll() {
    return this.http.requestGet('api/report/getveeam');
  }

}
