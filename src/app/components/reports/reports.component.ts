import { Response } from '@angular/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ReportsService } from './reports.service';
declare let $;
@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss'],
  providers: [ReportsService]
})
export class ReportsComponent implements OnInit {
  ReportDataAlls: any = [];
  ReportDataDetail: any = [];
  flagCurrent: any;
  formReport: FormGroup;
  ReportVeeamAlls = {};

  datastatus = {
   pa: 'paid',
    ac: 'active',
    rm: 'remove',
    up: 'unpaid',
    au: 'unpaid_active',
    ov: 'overdue',
    ao: 'overdue_active',
    et: 'Enterprise'
  }

  constructor(builder: FormBuilder, private service: ReportsService) {
    this.formReport = builder.group({
      cust_id: [''],
      pck_status: ['']
    });
  }

  PackagesAll() {
    this.service.PostPackagesAll().subscribe(res => {
      this.ReportDataAlls = res;
    });
  }

  btnReportFilters() {
    this.service.PostReportFilters(this.formReport.value).subscribe(res => {
      this.ReportDataAlls = res;
      //console.log(res)
    });
  }


  ReportVeeamAll(): void {
    this
      .service
      .GetveeamAll()
      .subscribe((res:any) => {
        this.ReportVeeamAlls = res.data;
      });
  }


  ngOnInit() {
    this.ReportVeeamAll();
  }

  exportData() {
    let result: string = "List of Records\r\nPackage No, Type, Status, VM, Storage All, Storage Used, Start Date, End Date, Client ID, Name, Family, Company\r\n";
    this.ReportDataAlls.data.forEach(element => {
      result += (element.pck_id + ", " + element.pck_type_name + ", " + element.pck_status + ", " + element.vm + ", "
        + element.storage + ", " + "" + ", " + element.pck_start_dt + ", " + element.pck_end_dt + ", "
        + element.cust_id + ", " + element.firstname + ", " + element.lastname + ", " + element.company_name + "\r\n");
    });
    var element = document.createElement('a'); 1
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(result));
    element.setAttribute('download', 'Report.csv');
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }

  expands(id: string) {
    if ($('.detail-' + id).css('display') == 'none') {
      $('.item-set').css('background-color', '#fff');
      $('.detail-set').hide();
      $('.detail-' + id).css('opacity', 0)
        .slideDown(1000)
        .animate(
        { opacity: 1 },
        { queue: false, duration: 'slow' }
        );
      $('.item-' + id).css('background-color', '#dbeddb');
    } else {
      $('.item-' + id).css('background-color', '#fff');
      $('.detail-set').hide();
    }
  }

  getDetailReport(TenantName: string, pck_id: number) {
    this.ReportDataDetail = [];
    this.service.PostReportDetails(TenantName).subscribe((res: any) => {
      this.ReportDataDetail = res;
    });
    this.expands(TenantName);
  }
}