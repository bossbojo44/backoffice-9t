import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../services/http.service';
declare const $;

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css']
})
export class ChatbotComponent implements OnInit {

  constructor(
    private Http: HttpService
  ) {
    this.showUPdate();
    this.showTestTextChatBot();
  }
  container: HTMLElement;
  show: any;
  User: any;
  Atextnow: any;
  ChatAll: Allchat[];
  Users: Users[];
  History: History[]


  ngOnInit() { }

  //----------call function-------
  showUPdate() {//เปลี่ยนค่าการเเสดง
    this.ChatAll = [];
    var obj2 = {
      Qtext: '',
      Atext: ''
    };
    this.Http.requestPost('api/GetChatbot', obj2).subscribe(is => {
      this.show = is;
      for (var i = 0; i < this.show.data.length; i++) {
        //console.log('Q: ' + this.show.data[i][1].Value + 'A: ' + this.show.data[i][2].Value);
        this.ChatAll.push(new Allchat(this.show.data[i][0].Value, this.show.data[i][1].Value, this.show.data[i][2].Value));
      }
    });
  }
  showTestTextChatBot() {//เปลี่ยนค่าการเเสดงการเเชท bot
    this.History = [];
    var obj2 = { Qtext: '', Atext: '' };
    this.Http.requestPost('api/GetHistory', obj2).subscribe(is => {
      this.show = is;
      console.log(this.show);
      for (var i = 0; i < this.show.data.length; i++) {
        //console.log('Q: ' + this.show.data[i][1].Value + 'A: ' + this.show.data[i][2].Value);
        this.History.push(new History(this.show.data[i][1].Value, this.show.data[i][2].Value));
      }
    });
    this.onscroll();
  }
  senddata(Q: any, A: any) {//เพิ่มคำถามเเละคำตอบ
    console.log("add data");
    var obj = {
      Qtext: Q.value,
      Atext: A.value
    };
    this.Http.requestPost('api/AddChatBot', obj).subscribe(is => {
      //console.log(is)
      this.showUPdate();
    });
  }
  deleteChat(Id: string) {//ลบคำถามเเละคำตอบ
    console.log(Id);
    var obj = {
      ID: Id
    };
    this.Http.requestPost('api/DeleteChatbot', obj).subscribe(is => {
      //console.log(is);
      this.showUPdate();
    });

  }
  editChatBot(ID: any, Qtext: any, Atext: any) {//เเก้ไขคำถามเเละคำตอบ
    var obj = {
      Id: ID,
      Qtext: Qtext.value,
      Atext: Atext.value
    };
    this.Http.requestPost('api/EditChatBot', obj).subscribe(is => {
      console.log(is)
      this.showUPdate();
    });
  }
  GetStartForChat(Topic: any) {//เริ่มการสนทนา
    var Qtext = Topic.value;
    var Atext = this.AutoAnswer(Topic.value);
    console.log(Atext);
    if (Atext != 'wait') {
      var obj = {
        Topic: Topic.value,
        Atext: Atext
      };
      console.log(obj);
      this.Http.requestPost('api/AddHistory', obj).subscribe(is => {
        console.log(is)
        this.showTestTextChatBot();
        this.onscroll();
      });
    }
    $('#Qnow').val('');
  }
  AutoAnswer(Q: any) {
    var retuAtext;
    var strhtml = '';
    for (var index = 0; index < this.ChatAll.length; index++) {
      var Qall = this.ChatAll[index].Qtext.split(",");
      for (var i = 0; i < Qall.length; i++) {
        if (Q.toLowerCase() == Qall[i].toLowerCase()) {
          retuAtext = this.ChatAll[index].Atext.split(",");
          if (retuAtext.length > 1) {
            for (var j = 0; j < retuAtext.length; j++) {
              strhtml += `<div class="radio">
                            <label><input type="radio" name="Qchoose" value="`+ retuAtext[j] + `">` + retuAtext[j] + `</label>
                          </div>`;
            }
            $('#choseBot').css("display", "block");
            $("#Qnow").prop('disabled', true);
            $('#myForm').html(strhtml);
            return 'wait';
          } else {
            return retuAtext[0];
          }
        }
      }
    }
    return 'Don\'t have reply because not knowledge';
  }
  getQ() {
    var getQ = $('input[name=Qchoose]:checked', '#myForm').val();
    console.log(getQ);
    var obj = {
      Topic: getQ,
      Atext: this.AutoAnswer(getQ)
    };
    console.log(obj);
    this.Http.requestPost('api/AddHistory', obj).subscribe(is => {
      console.log(is)
      this.showTestTextChatBot();
      this.onscroll();
    });
    $("#Qnow").prop('disabled', false);
    $('#choseBot').css("display", "none");
  }
  //จัดการ CSS 
  showEditModal(Id: String) {
    console.log(Id);
    $('#' + Id).css("display", "block");
  }
  CloseModel(Id: String) {
    $('#' + Id).css("display", "none");
  }
  showtestBot() {
    this.onscroll();
    $('#testChat').css("display", "block");
  }
  closetestBot() {
    $('#testChat').css("display", "none");
  }
  onscroll() {
    this.container = <HTMLElement>document.querySelector('.chat-group');
    // init element
    setTimeout(() => {
      if (!this.container) return;
      $(this.container).animate({ scrollTop: this.container.scrollHeight * 10 })
     // $(this.container).scrollTop($(this.container)[0].scrollHeight);
    });
  }
}

export class Allchat {
  constructor(Id: string, Q: string, A: string) {
    this.Id = Id;
    this.Qtext = Q;
    this.Atext = A;
  }
  Id: String;
  Qtext: String;
  Atext: String;
}
export class Users {
  constructor(Id: string, name: string, email: string) {
    this.Id = Id;
    this.name = name;
    this.email = email;
  }
  Id: String;
  name: String;
  email: String;
}
export class History {
  constructor(Q: string, A: string) {
    this.Qtext = Q;
    this.Atext = A;
  }
  Qtext: String;
  Atext: String;
}

