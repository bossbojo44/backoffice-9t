import { HttpService } from './../../services/http.service';
import { Injectable } from '@angular/core';

@Injectable()
export class ChatAutoService {

   constructor(private http: HttpService) { }

   readRateInfo(requestData: any){
     return this.http.requestPost('api/read-score', requestData);
   }

   addSentence(sentenceData: any){
     return this.http.requestPost('api/new-sentence',sentenceData);
   }

   ReadSentence(requestData: any){
     return this.http.requestPost('api/auto-sentences',requestData);
   }

   updateSentence(updateData: any){
     return this.http.requestPost('api/update-sentence',updateData);
   }

   deleteSentence(deleteData: any){
     return this.http.requestPost('api/delete-sentence',deleteData);
   }

   assign(assignData: any){
     return this.http.requestPost('api/assign', assignData);
   }
   updateAssign(permission: any){
     return this.http.requestPost('api/updateAssign', permission);
   }

    getMap(ip: any){
     return this.http.requestPost('api/map', ip);
   }
  
}
