﻿import { style } from '@angular/animations';
import { FormGroup, FormBuilder } from '@angular/forms';
import { element } from 'protractor';
import { ChatAutoService } from './chat-auto.service';
import { Component, OnInit, OnDestroy, ChangeDetectorRef } from '@angular/core';
import { SignalRService, SignalRModel } from '../../services/SignalR.service';
import { AuthenticationService } from '../../services/authentication.service';
import {
 ChatsService,UserType, UserRoom, MessageHub, UserConnect, UserLog, Greeting, Payment,
    Setting, Pricing, Finishing
} from './chats.service';
import { Subscription } from "rxjs/Subscription";
import { UsersHub } from './chats.service';
import { VerifyService } from '../../services/verify.service';
declare let $;
@Component({
    selector: 'app-chats',
    templateUrl: './chats.component.html',
    styleUrls: ['./chats.component.scss'],
    providers: [ChatAutoService]
})
export class ChatsComponent implements OnInit, OnDestroy {
    public AnswerOption: string = "";
    public AnswerSentence: string;
    public GreetingDataAlls: Greeting[] = [];
    public PaymentDataAlls: Payment[] = [];
    public SettingDataAlls: Setting[] = [];
    public PricingDataAlls: Pricing[] = [];
    public FinishingDataAlls: Finishing[] = [];
    public rateCount: number;
    public notRateCount: number;
    public scoreAvg: string;
    public check: boolean = true;
    public active: boolean = true;
    public last_message: MessageHub;

    constructor(private serviceChatAuto: ChatAutoService, public service: ChatsService, private authen: AuthenticationService, private detector: ChangeDetectorRef, private verifyService: VerifyService) {
        this.readSentences();
        this.readRateInfo();
    }

    openAvaible() {
        if ($('.group-answers').is(':visible')) {
            $(".group-answers").hide("slide", { direction: "right" }, 1000);
        }
        else {
            $('.group-answers').toggle('slide', {
                direction: 'right'
            }, 1000, function () { $('.group-answers').fadeIn(); });
        }
    }

    readSentences() {
        let jsonRequest = { "request": "addlink" };

        this.serviceChatAuto.ReadSentence(jsonRequest).subscribe((res: any) => {
            //console.log(res);
            this.GreetingDataAlls = [];
            this.PaymentDataAlls = [];
            this.SettingDataAlls = [];
            this.PricingDataAlls = [];
            this.FinishingDataAlls = [];
            for (let i: number = 0; i < res.data.greeting.length; i++)
                this.GreetingDataAlls.push(new Greeting(res.data.greeting[i][1].Value));

            for (let i: number = 0; i < res.data.payment.length; i++)
                this.PaymentDataAlls.push(new Payment(res.data.payment[i][1].Value));

            for (let i: number = 0; i < res.data.setting.length; i++)
                this.SettingDataAlls.push(new Setting(res.data.setting[i][1].Value));

            for (let i: number = 0; i < res.data.pricing.length; i++)
                this.PricingDataAlls.push(new Pricing(res.data.pricing[i][1].Value));

            for (let i: number = 0; i < res.data.finishing.length; i++)
                this.FinishingDataAlls.push(new Finishing(res.data.finishing[i][1].Value));
        });
    }

    readRateInfo() {
        let jsonRequest = { "request": "addlink" };

        this.serviceChatAuto.readRateInfo(jsonRequest).subscribe((res: any) => {

            let sum: number = 0;
            for (let i: number = 0; i < res.data.rate.length; i++) {
                sum = sum + res.data.rate[i][2].Value;
            }

            if (res.data.rate.length == 0)
                this.scoreAvg = '0';
            else
                this.scoreAvg = (sum / res.data.rate.length).toFixed(2);

            this.rateCount = res.data.rate.length;
            this.notRateCount = res.data.allSupport.length - res.data.rate.length;
        });
    }

    addNewSentence() {
        let jsonAddSentence = {
            "group": this.AnswerOption,
            "sentence": this.AnswerSentence
        }
        this.serviceChatAuto.addSentence(jsonAddSentence).subscribe((res: any) => {
            let newAns = (<HTMLInputElement>document.getElementById('autoAnswer'));
            newAns.value = "";
            this.readSentences();
        });

    }

    editSentence(data: any, group: any) {
        (<HTMLInputElement>document.getElementById('editAnswer')).value = data;
        this.AnswerOption = group;
        this.AnswerSentence = data;
    }

    submitUpdate(sentence: any) {
        let jsonUpdateSentence = {
            "group": this.AnswerOption,
            "sentence": this.AnswerSentence,
            "newSentence": sentence
        }
        this.serviceChatAuto.updateSentence(jsonUpdateSentence).subscribe((res: any) => {
            this.readSentences();
        });
    }

    submitDelete(sentence: any) {
        //console.log(this.AnswerOption);
        let jsonDeleteSentence = {
            "group": this.AnswerOption,
            "sentence": sentence
        }
        //console.log(jsonDeleteSentence);
        this.serviceChatAuto.deleteSentence(jsonDeleteSentence).subscribe((res: any) => {
            this.readSentences();
        });
    }

    submitNewSentence(word: string) {
        if (word != null && word != '' && this.AnswerOption != '') {
            this.AnswerSentence = word;
            this.addNewSentence();
        }
        else
            alert("Please select group and type new sentence!");

    }
    cancelNewSentence() {
        (<HTMLInputElement>document.getElementById('autoAnswer')).value = "";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.add('btn-default');
    }

    greetingAnswer() {
        this.AnswerOption = "greeting";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.remove('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.add('btn-default');
        let answer = (<HTMLInputElement>document.getElementById('autoAnswer'));
        answer.value = "Hello {{name}} {{fname}}";
    }
    paymentAnswer() {
        this.AnswerOption = "payment";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.remove('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.add('btn-default');
        let answer = (<HTMLInputElement>document.getElementById('autoAnswer'));
        answer.value = "Payment";
    }
    settingAnswer() {
        this.AnswerOption = "setting";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.remove('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.add('btn-default');
        let answer = (<HTMLInputElement>document.getElementById('autoAnswer'));
        answer.value = "Setting";
    }
    pricingAnswer() {
        this.AnswerOption = "pricing";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.remove('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.add('btn-default');
        let answer = (<HTMLInputElement>document.getElementById('autoAnswer'));
        answer.value = "Pricing";
    }
    finishingAnswer() {
        this.AnswerOption = "finishing";
        (<HTMLInputElement>document.getElementById('btn-greeting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-payment')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-setting')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-pricing')).classList.add('btn-default');
        (<HTMLInputElement>document.getElementById('btn-finishing')).classList.remove('btn-default');
        let answer = (<HTMLInputElement>document.getElementById('autoAnswer'));
        answer.value = "Finishing";
    }

    selectSentence(data: any) {
        let element = (<HTMLInputElement>document.getElementById('chatBox'));

        if (this.userInChat != undefined) {
            if ((this.userInChat.User.name == null || this.userInChat.User.name == "")) {
                let name = data.replace("{{name}}", "").replace("{{fname}}", "");
            }
            else {
                if (this.userInChat.Type == UserType.UnUser) {
                    let name = data.replace("{{name}}", this.userInChat.User.name).replace("{{fname}}", this.userInChat.User.name);
                }
                if (this.userInChat.Type == UserType.User) {
                    let name = data.replace("{{name}}", this.userInChat.Address.firstname).replace("{{fname}}", this.userInChat.Address.lastname);
                }
            }
            element.value = name;
        }
    }

    onPressCancel() {
        /*let jsonRequest = { "request": "addlink" };
            this.serviceChatAuto.updateAssign(jsonRequest).subscribe((res: any) => {
                console.log(res);
            });   */
    }

    JqueryFunc() {
        this.detector.detectChanges();
        $.fn.toggleSwitch = function (callback) {
            var id = this.attr("id"), switchDivId = id + "-switch";
            $("<div/>", { class: "onoffswitch", id: switchDivId }).insertAfter(this);
            var input = this.clone();
            $("div#" + switchDivId).append(input.addClass('onoffswitch-checkbox'));
            $("<label/>", { class: "onoffswitch-label", for: id }).appendTo("div#" + switchDivId);
            this.remove();
            input.change(() => {
                callback(input.prop('checked'));
            });
        };
        this.detector.detectChanges();
    }

    InnitialLoad() {

        if ($(window).width() <= 1024 && $(window).width() >= 0) {
                $(".group-answers").hide("slide", { direction: "right" }, 1000);
            }

        $(window).on('resize', function () {
            if ($(window).width() <= 1024 && $(window).width() >= 0) {
                $(".group-answers").hide("slide", { direction: "right" }, 1000);
            }

            if ($(window).width() > 1024){
                $('.group-answers').show('slide', {
                    direction: 'right'
                }, 1000, function () { $('.group-answers').fadeIn(); });
            }

        });

        this.JqueryFunc();
        $("#myonoffswitch1").toggleSwitch(e => {
            this.check = !this.check;
            this.active = true;
            var toggle = (<HTMLInputElement>document.getElementById('myonoffswitch1'));
            var header = (<HTMLInputElement>document.getElementById('op_status'));
            header.style.color = '#4c7a34';
            if (this.check == false) {
                if (this.usersYourchats_Active.length > 0) {
                    toggle.checked = true;
                    this.check = !this.check;
                    $('#inactiveModal').modal();
                }
                else {
                    header.style.color = 'red';
                    this.active = false;
                }
            }
        });
        $('.ca-menu > li:not(.clearmsg)').on('click', function () {
            $('.ca-menu > li').removeClass('active');
            $(this).addClass('active');
        });
        $('.ca-web > li:not(.clearmsg)').on('click', function () {
            $('.ca-web  > li').removeClass('active');
            $(this).addClass('active');
        });
    }

    ngOnInit() {
        this.InnitialLoad();
        this.onProcessChatBox();

        // set default chat
        this.service.usersYourchats.forEach(m => {
            if (m.IsChat) this.userInChat = Object.assign(new UsersHub(), m);
        });

        //this.place('101.51.225.98');
    }

    place(ip){

        let xx = [];
        let yy = '';
        let iip = {ip:ip};
       this.serviceChatAuto.getMap(iip).subscribe((res: any) => {
                xx = res.data.split(",");
                var lat = xx[0];
                var lng = xx[1];
                this.showmap(lat,lng)
        });
    }
    showmap(lat,lng){
         var url = 'http://www.google.com/maps/place/'+lat+','+lng;
        window.open(url, '_blank');
    }

    ngOnDestroy() {
        this.service.disconnection();
        this.service.usersYourchats = [];
        $(window).off('click');
    }

    // for chat resolve by loem 07/06/2017
    /* ============================================================= */
    /* ============================================================= */
    /* ============================================================= */

    container: HTMLElement;
    inKeying: boolean = false;
    inKeyTimeout: any;
    inReadingTimeout: any;
    isReading: boolean;

    UserType = UserType;
    Room = UserRoom;

    operators: UsersHub[] = [];
    usersIncomes: UsersHub[] = [];
    userInChat: UsersHub;
    logsChat: MessageHub[] = [];
    userAssigned: UsersHub;
    currentOps: UsersHub | any = {};
    usersYourchats_Active: UsersHub[] | any = {};

    // Assign to operator
    onAssignToOperator(dataOp: UsersHub) {
        this.service.Hub.server.op_assign2op(dataOp.UserRef, this.userInChat.ConnectId);
        this.service.usersYourchats.splice(this.service.usersYourchats.findIndex(m => m.ConnectId == this.userInChat.ConnectId), 1);
        this.userInChat = null;
        $('#assignModal').modal('hide');
        this.detector.detectChanges();
    }

    // onselect user to your chat
    onSelectYourChatBox(user: UsersHub) {
        user.InChat = this.Room.Chat;
        user.Messages.map(m => m.UserType = this.UserType.User);
        this.service.usersYourchats.push(Object.assign(new UsersHub(), user));
        this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);
        this.service.Hub.server.op_connectToClient(this.currentOps.UserRef, user.ConnectId);
        this.detector.detectChanges();
        // log of user not login
        //this.service.Hub.server.showUserLogNotLogin(user.ConnectId);
        // start chat

        this.verifyService.setActiveChats(this.usersYourchats_Active.length);

        //console.log("check user=> " + this.usersYourchats_Active.length);
        this.usersYourchats_Active.forEach(element => {
            //console.log(element);
        });
        this.onStartChat(user);
        //console.log("length usersYourchats => " + this.service.usersYourchats.length);
    }

    // on start chat and push to chat box
    onStartChat(user: UsersHub) {
        // check window focus or blur

        this.onProcessWindowFocus();
        // user data user to chat room
        this.service.usersYourchats.map(m => {
            if (m.ConnectId == user.ConnectId) {
                if (!m.StartChat)
                    m.StartChat = new Date();
                m.IsChat = true;
                m.LastMessagesCount = this.userMessages(m.Messages).length;
                this.userInChat = Object.assign(new UsersHub(), m);
                this.onReading();
            }
            else {
                m.IsChat = false;
            }
            return m;
        });

        this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);
        this.processScrollTop();
        this.detector.detectChanges();
        this.verifyService.setActiveChats(this.usersYourchats_Active.length);
    }

    // operator send message
    onOpSendMessage(message: HTMLInputElement) {
        if (this.userInChat == null) return;
        if (message.value.trim() == '') return;
        this.service.Hub.server.op_sendMessage(this.userInChat.ConnectId, message.value);
        message.value = '';
    }

    // calculate count messages of user
    userMessages(messages: MessageHub[]) {
        return messages.filter(m => m.UserType == this.UserType.User);
    }

    // onchat room
    oncloseChatRoom() {
        if (!this.userInChat) return;

        /*var lastMessage = (<HTMLInputElement>document.getElementById('chatBox'));
        lastMessage.value = "Hope your problem was solved. Also you can rate my work and five star for chosing";
        console.log("test last message => "+lastMessage);
        this.onOpSendMessage(lastMessage);*/
        if(this.last_message != undefined)
            this.service.Hub.server.op_ask4rating(this.currentOps.UserRef, this.userInChat.ConnectId);

        this.service.usersYourchats.map(m => {
            if (m.ConnectId == this.userInChat.ConnectId)
                m.IsChat = false;
            return m;
        });
        this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);
        this.userInChat = null;
        this.last_message = undefined;
        this.detector.detectChanges();
        this.verifyService.setActiveChats(this.usersYourchats_Active.length);
    }

    // on user keying message
    onOpKeying(event: KeyboardEvent) {
        if (!this.userInChat) return;
        const key = event.keyCode || event.charCode;
        const allowKeys = [13];
        if (allowKeys.indexOf(key) !== -1) return;
        this.service.Hub.server.onOpKeying(this.userInChat.ConnectId);
    }

    // on reading message
    onReading() {
        if (!this.userInChat || !this.isReading) return;
        clearTimeout(this.inReadingTimeout);
        this.inReadingTimeout = setTimeout(() => {
            this.service.Hub.server.onReading(this.userInChat.ConnectId);
        }, 500);
    }

    // on show history log
    onShowHistoryLog(user: UsersHub) {
        this.service.Hub.server.showUserLogById(user.UserRef);
    }

    // process scrollbar for chat
    private processScrollTop() {
        this.container = <HTMLElement>document.querySelector('.chat-group');
        // init element
        setTimeout(() => {
            if (!this.container) return;
            $(this.container).animate({ scrollTop: this.container.scrollHeight })
        });
    }

    private saveUserLog(ChatRoom: string, Message: MessageHub, readed: string = "") {
        this.service.Hub.server.saveUserLog(ChatRoom, Message, readed);
    }

    // all process on chat box
    private onProcessChatBox() {
        // get modal element
        const historyModal = $('#historyChatModal');


        // response logof user not login
        this.service.Hub.client.showUserLogNotLogin = (res: UserLog[]) => {
            if (!this.userInChat || res.length == 0) return;
            const userMessages = this.userInChat.Messages;
            // this.userInChat.Messages = [];
            res.forEach(m => {
                if (m.UserType == UserType.Operator) {
                    let message = new MessageHub(m.Message, m.Time, m.User);
                    message.UserType = m.UserType;
                    userMessages.push(message);
                }
            });
            userMessages.sort((a: MessageHub, b: MessageHub) => Date.parse(a.Time.toString()) - Date.parse(b.Time.toString()));
            this.detector.detectChanges();
        };

        // response history message logs
        this.service.Hub.client.showUserLog = (res: UserLog[]) => {
            if (!this.userInChat) return;
            this.logsChat = [];
            res.forEach(m => {
                let message = new MessageHub();
                message.ConnectId = m.ChatRoom;
                message.Message = m.Message;
                message.Time = m.Time;
                message.UserType = m.UserType;
                message.User = m.User;
                this.logsChat.push(message);
            });
            // open modal
            historyModal.modal();
            this.detector.detectChanges();
        }

        // response if client uesr enter keyboard
        this.service.Hub.client.onKeying = (ConnectId) => {
            if (!this.userInChat) return;
            if (this.userInChat.ConnectId == ConnectId) {
                clearTimeout(this.inKeyTimeout);
                this.inKeying = true;
                this.inKeyTimeout = setTimeout(() => {
                    this.inKeying = false;
                    this.detector.detectChanges();
                }, 500);
            }
            this.detector.detectChanges();
        };

        // response from server from operator send message
        this.service.Hub.client.op_sendMessage = (res: MessageHub) => {
            if (this.userInChat) {
                this.last_message = res;
                res.UserType = UserType.Operator;
                this.userInChat.Messages.push(res);
            }

            // save history log
            this.saveUserLog(this.userInChat.ConnectId, res);

            // refresh event
            this.detector.detectChanges();
            this.processScrollTop();
        };

        // response from send message from 
        this.service.Hub.client.sendMessage = (res: MessageHub) => {
            // for user map message
            if (this.service.usersYourchats.filter(m => m.ConnectId == res.ConnectId).length !== 0) {
                // Your chat
                this.service.usersYourchats.map(m => {
                    if (m.ConnectId == res.ConnectId) {
                        res.UserType = UserType.User;
                        m.Messages.push(res);

                        // Update LastMessagesCount in the room
                        if (this.userInChat && this.userInChat.ConnectId === res.ConnectId) {
                            m.LastMessagesCount = this.userMessages(m.Messages).length;
                            this.onReading();
                        }
                    }
                    return m;
                });
            }
            else if (this.usersIncomes.filter(m => m.ConnectId == res.ConnectId)) {
                // Incomming 
                this.usersIncomes.map(m => {
                    if (m.ConnectId == res.ConnectId) {
                        res.UserType = UserType.User;
                        m.Messages.push(res);
                    }
                    return m;
                });
            }
            this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);

            // save history message
            this.saveUserLog(res.ConnectId, res);

            // refresh event 
            this.detector.detectChanges();
            this.verifyService.setActiveChats(this.usersYourchats_Active.length);
            this.processScrollTop();
        };

        // response from server from users login all
        this.service.Hub.client.onUserLoginAll = (res: UsersHub[]) => {
            // user income
            this.usersIncomes = res.filter(m => {
                if (m.Type == UserType.User || m.Type == UserType.UnUser) {
                    m.InChat = UserRoom.Income;
                    // check from user your chats
                    this.service.usersYourchats.filter(s => {
                        if (s.ConnectId == m.ConnectId)
                            m.InChat = null;
                        return s;
                    });
                    m.Messages.map(m => m.UserType = this.UserType.User);
                    return m;
                }
            });
            // refresh event
            this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);
            this.detector.detectChanges();
            this.verifyService.setActiveChats(this.usersYourchats_Active.length);
        };

        // response from queue of clients
        this.service.Hub.client.userLoginByQueues = (res: UsersHub[]) => {
            // เพิ่มผู้ใช้งานเข้าไป
            this.service.Hub.client.onUserLoginAll(res);
            // refresh event
            this.detector.detectChanges();
        };

        // response from operator login all
        this.service.Hub.client.onOperatorLoginAll = (res: UsersHub[]) => {
            // // oparator
            this.operators = res.filter(m => m.Type == UserType.Operator);
            // refresh event
            this.detector.detectChanges();
        };

        // response from server when operator logon
        this.service.Hub.client.onOperatorLogin = (res: UsersHub) => {
            this.currentOps = res;
        };

        // response from user logout
        this.service.Hub.client.onUserLogoutAll = (ConnectId: string) => {
            this.service.usersYourchats.map(m => {
                if (m.ConnectId == ConnectId) {
                    m.StopChat = new Date();
                    this.readRateInfo();
                }
                return m;
            });
            if (this.userInChat && ConnectId == this.userInChat.ConnectId) {
                this.userInChat.StopChat = new Date();
                this.service.Hub.server.op_ask4rating(this.currentOps.UserRef, this.userInChat.ConnectId);
                this.readRateInfo();
            }
            const findIndex = this.usersIncomes.findIndex(m => m.ConnectId == ConnectId);
            if (findIndex >= 0)
                this.usersIncomes.splice(findIndex, 1);

            //console.log("check user => " + this.service.usersYourchats.length);
            this.usersYourchats_Active = this.service.usersYourchats.filter(element => element.StopChat == null);
            //console.log(this.usersYourchats_Active);

            this.detector.detectChanges();
            this.verifyService.setActiveChats(this.usersYourchats_Active.length);
        };

        // request to server for login 
        this.service.connection.then(() => {
            this.service.Hub.server.onOperatorLogin(this.authen.getAuthenticated);
        });



    }

    // check window is focus or blur
    private onProcessWindowFocus() {
        // check focus
        this.isReading = true;
        $(window)
            .focus(() => {
                this.isReading = true;
            })
            .blur(() => {
                this.isReading = false;
            })
            .click(e => {
                this.onReading();
                e.stopPropagation();
            });
    }
}