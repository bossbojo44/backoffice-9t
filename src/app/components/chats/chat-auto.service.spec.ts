import { TestBed, inject } from '@angular/core/testing';

import { ChatAutoService } from './chat-auto.service';

describe('ChatAutoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ChatAutoService]
    });
  });

  it('should ...', inject([ChatAutoService], (service: ChatAutoService) => {
    expect(service).toBeTruthy();
  }));
});
