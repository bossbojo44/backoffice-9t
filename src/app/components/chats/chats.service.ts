import { Injectable, ChangeDetectorRef } from '@angular/core';
import { SignalRModel, SignalRService } from '../../services/SignalR.service';
import { AuthenticationService } from '../../services/authentication.service';
import { Subject } from 'rxjs/Subject';
import { baseUrl } from '../../configs/url.config';
@Injectable()
export class ChatsService {
    private hubUrl = baseUrl + '/signalr';
    private hubName = 'chatHub';
    public Hub: SignalRModel;
    public hubConnection: any;
    public usersYourchats: UsersHub[] = [];

    constructor(private signalR: SignalRService) {
        this.processScriptURL();
        this.Hub = this.signalR.connection[this.hubName];
        this.hubConnection = this.Hub.connection;
        this.hubConnection.url = this.hubUrl;
    }

    get connection(): Promise<any> {
        return new Promise(resolve => {
            this.disconnection();
            this.hubConnection.start().done(connect => resolve(connect));
        });
    }

    disconnection() {
        this.hubConnection.stop();
    }

    private processScriptURL() {
        let scriptURL = <HTMLScriptElement>document.getElementById('signleR-url');
        if (scriptURL) this.hubUrl = scriptURL.src.replace('/hubs', '');
    }
}

export class UsersHub {
    public ConnectId: string;
    public Type: number;
    public UserRef: number;
    public User: UserConnect;
    public Messages: Array<MessageHub>;
    public Address: Address;
    public IpAddress: string;

    InChat: string;
    IsChat: boolean;
    LastMessagesCount: number = 0;
    StartChat: Date;
    StopChat: Date;
}

export class UserConnect {
    public email: string;
    public name: string;
    public topic: string;
}

export class MessageHub {
    public ConnectId: string;
    public Message: string;
    public Time: Date;
    public User: UserConnect;

    constructor(Message: string = null, Time: Date = new Date(), User = new UserConnect()) {
        this.Message = Message;
        this.Time = Time;
        this.User = User;
        this.UserType = UserType.User;
    }

    UserType: number;
}

export class UserType {
    static Operator: number = 2;
    static User: number = 1;
    static UnUser: number = 0;
}

export class UserRoom {
    static Income = 'Incomming';
    static Chat = 'YourChat';
}

export class Address {
    cust_id: number;
    company_name: string;
    firstname: string;
    lastname: string;
    phone_num: string;
    address1: string;
    country: string;
    province: string;
    post_code: string;
}

export class UserLog {
    _id: string;
    ChatRoom: string;
    UserRef: number;
    OperatorRef: number;
    Message: string;
    Time: Date;
    UserType: number;
    Readed: boolean;
    User: UserConnect;
}

export class Greeting {
    sentence: string;
    group: string = "greeting";
    constructor(_sentence: string) {
        this.sentence = _sentence;
    }
}

export class Payment {
    sentence: string;
    group: string = "payment";
    constructor(_sentence: string) {
        this.sentence = _sentence;
    }
}

export class Setting {
    sentence: string;
    group: string = "setting";
    constructor(_sentence: string) {
        this.sentence = _sentence;
    }
}

export class Pricing {
    sentence: string;
    group: string = "pricing";
    constructor(_sentence: string) {
        this.sentence = _sentence;
    }
}

export class Finishing {
    sentence: string;
    group: string = "finishing";
    constructor(_sentence: string) {
        this.sentence = _sentence;
    }
}
