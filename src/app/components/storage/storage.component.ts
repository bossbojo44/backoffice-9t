import { UsedStorage, NewStorage, Size, Status, StorageService } from './storage.service';
import { Component, OnInit } from '@angular/core';
declare let $;
@Component({
  selector: 'app-storage',
  templateUrl: './storage.component.html',
  styleUrls: ['./storage.component.scss'],
  providers: [StorageService]
})
export class StorageComponent implements OnInit {

  public used: UsedStorage[];
  public selected: UsedStorage[];
  public new: NewStorage[];
  public totalSpace: number;
  public usedSpace: number;
  public freeSpace: number;
  private totalSpaceHP: number;
  private usedSpaceHP: number;
  private freeSpaceHP: number;
  private totalSpaceQNAP: number;
  private usedSpaceQNAP: number;
  private freeSpaceQNAP: number;


  public usedBar: number;
  public freeBar: number;

  private checkHP:boolean;
  private checkQNAP:boolean;
  private checkDevice:boolean;
  

  constructor(private repo:StorageService) { 
    this.used = [];
    this.new = [];
    this.selected = [];

    this.totalSpace = 0;
    this.usedSpace = 0;
    this.freeSpace = 0;

    this.checkHP = true;
    this.checkQNAP = true;
    this.checkDevice = false;

    //this.used.push(new UsedStorage("SATA", "Seagate 5900 IronWolf [ST2000VN004] (Mock)", new Size("200", "500"), "HP Storageworks", new Status("Active", "Connected"), true));
    //this.used.push(new UsedStorage("SATA", "Seagate 5900 IronWolf [ST2000VN004] (Mock)", new Size("100", "500"), "QNAP", new Status("Active", "Connected"), true));

   // this.new.push(new NewStorage("SATA", "Seagate 5900 IronWolf [ST2000VN004]", 100, "HP Storageworks", new Status("Off", "Connected")));
   // this.new.push(new NewStorage("SATA", "Seagate 5900 IronWolf [ST2000VN004]", 100, "QNAP", new Status("Off", "Connected")));
    
   /* this.repo.readRepositories().subscribe((res:any) => {
      for(let i:number=0;i<res.data.length;i++){
        if(res.data[i].name.indexOf('HP') != -1){
        this.used.push(
          new UsedStorage("SATA", 
          res.data[i].name, 
          new Size((parseFloat(res.data[i].capacity)-parseFloat(res.data[i].freeSpace)).toFixed(2), parseFloat(res.data[i].capacity).toFixed(2)), 
          "HP Storageworks", 
          new Status("Active", "Connected"),
          true));
        }

        else if(res.data[i].name.indexOf('QNAP') != -1){
          this.used.push(
          new UsedStorage("SATA", 
          res.data[i].name, 
          new Size((parseFloat(res.data[i].capacity)-parseFloat(res.data[i].freeSpace)).toFixed(2), parseFloat(res.data[i].capacity).toFixed(2)), 
          "QNAP", 
          new Status("Active", "Connected"),
          true));
        }
      }

      for(let i:number=0;i<this.used.length;i++){
        this.totalSpace = this.totalSpace + parseFloat(this.used[i].size.total);
        this.usedSpace = this.usedSpace + parseFloat(this.used[i].size.used);
      }
        //console.log(this.totalSpace);
       // console.log(this.usedSpace);
        this.freeSpace = this.totalSpace - this.usedSpace;
      //  console.log(this.freeSpace);
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
    }); */

    this.Initialize();
  }

  ngOnInit() {
   /* $("#menu-toggle").click(function (e) {
      e.preventDefault();
      $("#wrapper").toggleClass("active");
      $("#sidebar").toggleClass("disable");
    });*/
  }
  Initialize() {
    $(document)
      .ready(function () {
        var $menuLeft = $('.pushmenu-left');
        var $nav_list = $('#nav_list');
        $nav_list.click(function () {
          $(this).toggleClass('active');
          $("#nav_list").hasClass('active')
            ? $(".buttonset").css({ 'width': '300px', 'border-bottom-right-radius': '0px' })
            : $(".buttonset").css({ 'width': '109px', 'border-bottom-right-radius': '30px' });

          $menuLeft.toggleClass('pushmenu-open');
        });
      });
  }

  deviceView(check:any, data:UsedStorage){

    this.totalSpaceQNAP = 0;
    this.usedSpaceQNAP = 0;
    this.freeSpaceQNAP = 0;
    this.totalSpaceHP = 0;
    this.usedSpaceHP = 0;
    this.freeSpaceHP = 0;
    this.totalSpace = 0;
    this.usedSpace = 0;
    this.freeSpace = 0;
    this.checkDevice = check.checked;

    if(this.checkDevice == true){
      this.selected.push(new UsedStorage(data.type, data.device, data.size, data.box, data.status, data.show));
     // console.log('add');
     // console.log(this.selected.length);
      for(let i:number=0;i<this.selected.length;i++){
        this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
        this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
      }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
    }
    else{
      this.selected.splice(this.selected.findIndex( s => s.device == data.device),1);
    //  console.log('remove');
   //   console.log(this.selected.length);
      if(this.selected.length > 0){
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else{
      //  console.log('used num => '+this.used.length);
     //   console.log(this.used);
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].show == true){
            this.totalSpace = this.totalSpace + parseFloat(this.used[i].size.total);
            this.usedSpace = this.usedSpace + parseFloat(this.used[i].size.used);
          }
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }
  }

  HPview(data:any){
    this.totalSpaceQNAP = 0;
    this.usedSpaceQNAP = 0;
    this.freeSpaceQNAP = 0;
    this.totalSpaceHP = 0;
    this.usedSpaceHP = 0;
    this.freeSpaceHP = 0;
    this.totalSpace = 0;
    this.usedSpace = 0;
    this.freeSpace = 0;
    this.checkHP = data.checked;

    if(this.checkHP && !this.checkQNAP){
      this.selected = this.selected.filter(s => s.box !== "QNAP");
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
            if(this.used[i].box === "HP Storageworks"){
              this.totalSpaceHP = this.totalSpaceHP + parseFloat(this.used[i].size.total);
              this.usedSpaceHP = this.usedSpaceHP + parseFloat(this.used[i].size.used);
              this.used[i].show = true;
            }
            else if(this.used[i].box === "QNAP"){
              this.used[i].show = false;
            }
        }
        this.selected = this.selected.filter(s => s.box !== "QNAP");
      //  console.log('remove QNAP = > '+this.selected.length);

        this.freeSpaceHP = this.totalSpaceHP - this.usedSpaceHP;

        this.totalSpace = this.totalSpaceHP;
        this.usedSpace = this.usedSpaceHP;
        this.freeSpace = this.freeSpaceHP;

        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else if(this.selected.length > 0){
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "QNAP"){
            this.used[i].show = false;
          }
        }
        this.selected = this.selected.filter(s => s.box !== "QNAP");
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }

    else if(!this.checkHP && this.checkQNAP){
      this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "QNAP"){
            this.totalSpaceQNAP = this.totalSpaceQNAP + parseFloat(this.used[i].size.total);
            this.usedSpaceQNAP = this.usedSpaceQNAP + parseFloat(this.used[i].size.used);
            this.used[i].show = true;
          }
          else if(this.used[i].box === "HP Storageworks"){
            this.used[i].show = false;
          }
        }
        this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
      //  console.log('remove hp = > '+this.selected.length);

        this.freeSpaceQNAP = this.totalSpaceQNAP - this.usedSpaceQNAP;

        this.totalSpace = this.totalSpaceQNAP;
        this.usedSpace = this.usedSpaceQNAP;
        this.freeSpace = this.freeSpaceQNAP;

        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else if(this.selected.length > 0){
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "HP Storageworks"){
            this.used[i].show = false;
          }
        }
        this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }

    else if(this.checkHP && this.checkQNAP){
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
            if(this.used[i].box === "HP Storageworks"){
              this.totalSpaceHP = this.totalSpaceHP + parseFloat(this.used[i].size.total);
              this.usedSpaceHP = this.usedSpaceHP + parseFloat(this.used[i].size.used);
              this.used[i].show = true;
            }
        }
        this.freeSpaceHP = this.totalSpaceHP - this.usedSpaceHP;

        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "QNAP"){
            this.totalSpaceQNAP = this.totalSpaceQNAP + parseFloat(this.used[i].size.total);
            this.usedSpaceQNAP = this.usedSpaceQNAP + parseFloat(this.used[i].size.used);
            this.used[i].show = true;
          }
        }

        this.freeSpaceQNAP = this.totalSpaceQNAP - this.usedSpaceQNAP;

        var ts = this.totalSpaceHP+this.totalSpaceQNAP;
        var tu = this.usedSpaceHP+this.usedSpaceQNAP
        
        this.totalSpace = ts;
        this.freeSpace = ts - tu;
        this.usedSpace = tu

        this.usedBar = (tu/ts)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else if(this.selected.length > 0){
      //  console.log('select length => '+this.selected.length);
        for(let i:number=0;i<this.used.length;i++){
          this.used[i].show = true;
        }
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }

    else if(!this.checkHP && !this.checkQNAP){
      for(let i:number=0;i<this.used.length;i++)
        this.used[i].show = false;
      
      this.selected = [];
      this.totalSpace = 0;
      this.usedSpace = 0;

        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = 100;
        this.freeBar = 0;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
    }
  }

  QNAPview(data:any){
    this.totalSpaceQNAP = 0;
    this.usedSpaceQNAP = 0;
    this.freeSpaceQNAP = 0;
    this.totalSpaceHP = 0;
    this.usedSpaceHP = 0;
    this.freeSpaceHP = 0;
    this.totalSpace = 0;
    this.usedSpace = 0;
    this.freeSpace = 0;
    this.checkQNAP = data.checked;

    if(this.checkHP && !this.checkQNAP){
      this.selected = this.selected.filter(s => s.box !== "QNAP");
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
            if(this.used[i].box === "HP Storageworks"){
              this.totalSpaceHP = this.totalSpaceHP + parseFloat(this.used[i].size.total);
              this.usedSpaceHP = this.usedSpaceHP + parseFloat(this.used[i].size.used);
              this.used[i].show = true;
            }
            else if(this.used[i].box === "QNAP"){
              this.used[i].show = false;          
            }
        }
        this.selected = this.selected.filter(s => s.box !== "QNAP");
      //  console.log('remove qnap = > '+this.selected.length);
        this.freeSpaceHP = this.totalSpaceHP - this.usedSpaceHP;

        this.totalSpace = this.totalSpaceHP;
        this.usedSpace = this.usedSpaceHP;
        this.freeSpace = this.freeSpaceHP;

        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else if(this.selected.length > 0){
     //     console.log('enter here2');
          for(let i:number=0;i<this.used.length;i++){
            if(this.used[i].box === "QNAP"){
              this.used[i].show = false;
            }
          }
          this.selected = this.selected.filter(s => s.box !== "QNAP");
          for(let i:number=0;i<this.selected.length;i++){
            this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
            this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
          }
          this.freeSpace = this.totalSpace - this.usedSpace;
          this.usedBar = (this.usedSpace/this.totalSpace)*100;
          this.freeBar = 100-this.usedBar;
          (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
          (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
        }
    }

    else if(!this.checkHP && this.checkQNAP){
      this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "QNAP"){
            this.totalSpaceQNAP = this.totalSpaceQNAP + parseFloat(this.used[i].size.total);
            this.usedSpaceQNAP = this.usedSpaceQNAP + parseFloat(this.used[i].size.used);
            this.used[i].show = true;
          }
          else if(this.used[i].box === "HP Storageworks"){
            this.used[i].show = false;
          }
        }
        this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
      //  console.log('remove hp = > '+this.selected.length);
        this.freeSpaceQNAP = this.totalSpaceQNAP - this.usedSpaceQNAP;

        this.totalSpace = this.totalSpaceQNAP;
        this.usedSpace = this.usedSpaceQNAP;
        this.freeSpace = this.freeSpaceQNAP;

        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
      else if(this.selected.length > 0){
        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "HP Storageworks"){
            this.used[i].show = false;
          }
        }
        this.selected = this.selected.filter(s => s.box !== "HP Storageworks");
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }

    else if(this.checkHP && this.checkQNAP){
   //   console.log('select lenght => '+this.selected.length);
      if(this.selected.length < 1){
        for(let i:number=0;i<this.used.length;i++){
            if(this.used[i].box === "HP Storageworks"){
              this.totalSpaceHP = this.totalSpaceHP + parseFloat(this.used[i].size.total);
              this.usedSpaceHP = this.usedSpaceHP + parseFloat(this.used[i].size.used);
              this.used[i].show = true;
            }
        }
        this.freeSpaceHP = this.totalSpaceHP - this.usedSpaceHP;

        for(let i:number=0;i<this.used.length;i++){
          if(this.used[i].box === "QNAP"){
            this.totalSpaceQNAP = this.totalSpaceQNAP + parseFloat(this.used[i].size.total);
            this.usedSpaceQNAP = this.usedSpaceQNAP + parseFloat(this.used[i].size.used);
            this.used[i].show = true;
          }
        }
        this.freeSpaceQNAP = this.totalSpaceQNAP - this.usedSpaceQNAP;

        var ts = this.totalSpaceHP+this.totalSpaceQNAP;
        var tu = this.usedSpaceHP+this.usedSpaceQNAP

        this.totalSpace = ts;
        this.freeSpace = ts - tu;
        this.usedSpace = tu;

        this.usedBar = (tu/ts)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }

      else if(this.selected.length > 0){
        for(let i:number=0;i<this.used.length;i++){
          this.used[i].show = true;
        }
        for(let i:number=0;i<this.selected.length;i++){
          this.totalSpace = this.totalSpace + parseFloat(this.selected[i].size.total);
          this.usedSpace = this.usedSpace + parseFloat(this.selected[i].size.used);
        }
        this.freeSpace = this.totalSpace - this.usedSpace;
        this.usedBar = (this.usedSpace/this.totalSpace)*100;
        this.freeBar = 100-this.usedBar;
        (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
        (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
      }
    }

    else if(!this.checkHP && !this.checkQNAP){
      for(let i:number=0;i<this.used.length;i++)
        this.used[i].show = false;
    
      this.selected = [];
      
      this.totalSpace = 0;
      this.usedSpace = 0;

      this.freeSpace = this.totalSpace - this.usedSpace;
      this.usedBar = 100;
      this.freeBar = 0;
      (<HTMLInputElement>document.getElementById('usedBar')).style.width = this.usedBar.toString()+"%";
      (<HTMLInputElement>document.getElementById('freeBar')).style.width = this.freeBar.toString()+"%";
    }
  }

}





