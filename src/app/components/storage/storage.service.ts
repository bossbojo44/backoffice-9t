import { Injectable } from '@angular/core';
import { HttpService } from './../../services/http.service';

@Injectable()
export class StorageService {

  constructor(private http: HttpService) { }

  readRepositories(){
    return this.http.requestGet('api/storage');
  }

}

export class Size{
  public used:string;
  public total:string;
  constructor(used:string, total:string){
    this.used = used;
    this.total = total;
  }
}

export class Status{
  public active:string;
  public connection:string;
  constructor(active: string, connection: string){
    this.active = active;
    this.connection = connection;
  }
}

export class UsedStorage{
  public type:string;
  public device:string;
  public size:Size;
  public box:string;
  public status:Status;
  public show:boolean;
  constructor(type:string, device:string, size:Size, box:string, status: Status, show:boolean){
    this.type = type;
    this.device = device;
    this.size = size;
    this.box = box;
    this.status = status;
    this.show = show;
  }
}

export class NewStorage{
  public type:string;
  public device:string;
  public size:number;
  public box:string;
  public status:Status;
  constructor(type:string, device:string, size:number, box:string, status:Status){
    this.type = type;
    this.device = device;
    this.size = size;
    this.box = box;
    this.status = status;
  }
}
