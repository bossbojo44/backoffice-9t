import { Component, OnInit } from '@angular/core';
import { ChangePasswordService } from './change-password.service';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { HttpService } from '../../services/http.service';
declare let $;
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss'],
  providers: [ChangePasswordService]
})
export class ChangePasswordComponent implements OnInit {
  formChangePassword: FormGroup;
  formChangePin: FormGroup;
  message: any;
  BOT: any;
  data:any;
  message1: any;
  role:any = localStorage.getItem("Role");
  constructor(builder: FormBuilder, private service: ChangePasswordService , private Http: HttpService) {
    this.getStatusBot();
    this.formChangePassword = builder.group({
      old_password: [''],
      new_password: [''],
      confrim_password: ['']
    });
    this.formChangePin = builder.group({
      old_pin: builder.array([
        [''],
        [''],
        [''],
        [''],
        [''],
        ['']
      ]),
      new_pin: builder.array([
        [''],
        [''],
        [''],
        [''],
        [''],
        ['']
      ]),
      confrim_pin: builder.array([
        [''],
        [''],
        [''],
        [''],
        [''],
        ['']
      ])
    });
  }

  ngOnInit() {
    
  }
  getStatusBot(){
    var obj = {
      Qtext: "",
      Atext: ""
    };
    this.Http.requestPost('api/GetStatusBot', obj).subscribe(is => {
      this.data = is;
      //console.log(this.data.data[0][1].Value);
      this.data = this.data.data[0][1].Value;
    //  console.log(this.data);
      if(this.data == "On"){
        $('#myonoffswitch').prop('checked', true);
      }else{
        $('#myonoffswitch').prop('checked', false);
      }
      this.BOT = this.data;
    });
    
 //console.log(this.data);
    
    //return this.data;
  }
  OnchangeStatusBOT() {
    if ($('#myonoffswitch').is(':checked')) {
      this.BOT = "On";
      this.OnSendStatus("Off");
    } else {
      this.BOT = "Off";
      this.OnSendStatus("On");
    }
  }
  OnSendStatus(status: any) {
    var obj = {
      Qtext: status,
      Atext: ""
    };
    this.Http.requestPost('api/SetOnOffChatbot', obj).subscribe(is => {
      //console.log(is);
    });
  }
  onKeyin1(e, index) {
    console.log(index)
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs1-' + sumidndex).focus();
      }
    }
    else {
      var sumindex = index + 1;
      $('.inputs1-' + sumindex).focus();
    }
  }
  onKeydow1(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs1-' + sumidndex).focus();
      }
    }
  }
  onKeyin2(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs2-' + sumidndex).focus();
      }
    }
    else {
      var sumindex = index + 1;
      $('.inputs2-' + sumindex).focus();
    }
  }
  onKeydow2(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs2-' + sumidndex).focus();
      }
    }
  }
  onKeyin3(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs3-' + sumidndex).focus();
      }
    }
    else {
      var sumindex = index + 1;
      $('.inputs3-' + sumindex).focus();
    }
  }
  onKeydow3(e, index) {
    var key = e.keyCode || e.charCode;
    if (key == 8) {
      var sumidndex = index - 1;
      if (sumidndex >= 0 && sumidndex <= 5) {
        $('.inputs3-' + sumidndex).focus();
      }
    }
  }


  Change() {
    if (this.formChangePassword.controls.old_password.value == '' || this.formChangePassword.controls.new_password.value == '' || this.formChangePassword.controls.confrim_password.value == '') {
      this.message = 'data not null';
    }
    else if (this.formChangePassword.controls.new_password.value != this.formChangePassword.controls.confrim_password.value) {
      this.message = 'new password data did not match confrim password';
    }
    else {
      this.service.PostChangePassword(this.formChangePassword.value).subscribe((res: any) => {
        this.message = res.data;
      },
        err => {
          this.message = err.json().Message;
        });
    }
  }

  ChangePIN() {
    let model = this.formChangePin.value;
    let datapin = {
      old_pin: model.old_pin.join(''),
      new_pin: model.new_pin.join(''),
      confrim_pin: model.confrim_pin.join(''),
    }
    if (datapin.old_pin.length < 6) {
      this.message1 = "oldpin datanot null"
    }
    else if (datapin.new_pin.length < 6) {
      this.message1 = "newpin datanot null"
    }
    else if (datapin.confrim_pin.length < 6) {
      this.message1 = "confrimpin datanot null"
    } else {
      if (datapin.new_pin != datapin.confrim_pin) {
        this.message1 = "newPin not math confirmPin";
      } else {
        this.service.PostChangePin(datapin).subscribe((res: any) => {
          this.message1 = res.data;
        },
          err => {
            this.message1 = err.json().Message;
          });
      }
    }

  }

  get getFormArrayold_pin() {
    return (<FormArray>this.formChangePin.get('old_pin')).controls;
  }

  get getFormArraynew_pin() {
    return (<FormArray>this.formChangePin.get('new_pin')).controls;
  }

  get getFormArrayconfrim_pin() {
    return (<FormArray>this.formChangePin.get('confrim_pin')).controls;
  }

}
