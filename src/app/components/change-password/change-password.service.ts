import { Injectable } from '@angular/core';
import { HttpService } from '../../services/http.service';

@Injectable()
export class ChangePasswordService {

  constructor(private http:HttpService) { }


 PostChangePassword(model:any){
    return this.http.requestPost('api/Setting/ChangePassword',model);
  }
    PostChangePin(model:any){
    return this.http.requestPost('api/Setting/ChangePIN',model);
  }

}

