export class URLCurrent {
	static set(key: string, value: any): void {
		try {
			let val = value;
			if (typeof value == 'object') { val = JSON.stringify(value); }
			localStorage.setItem(key, val);
		} catch (e) { console.log('set local storage error : ', e); }
	}
	static get(key: string): any {
		var value = localStorage.getItem(key);
		try { value = JSON.parse(value); }
		catch (e) { }
		return value || null;
	}
	static RemoveByKey(key: string): any {
		localStorage.removeItem(key);		
	}

}