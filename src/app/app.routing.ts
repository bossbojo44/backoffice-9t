import { InvoicesComponent } from './components/packages/invoices/invoices.component';
import { VeeamReplicationComponent } from './components/packages/creates/veeam-replication/veeam-replication.component';
import { VeeamBackupComponent } from './components/packages/creates/veeam-backup/veeam-backup.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { StorageComponent } from './components/storage/storage.component';
import { ManagementComponent } from './components/management/management.component';
import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { LogsComponent } from './components/logs/logs.component';
import { AuthenticationGuard } from './guard/authentication.guard';
import { ReportsclientsComponent } from './components/clients/reports/reports.component';
import { ChatsComponent } from './components/chats/chats.component';
import { ReportsComponent } from './components/reports/reports.component';
import { PackagesComponent } from './components/packages/packages.component';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { UrlConfig } from './configs/url.config';
import { PaymentsComponent } from './components/payments/payments.component';
import { ClientsComponent } from './components/clients/clients.component';
import { UsersComponent } from './components/users/users.component';
import { AddnewusersComponent } from './components/addnewusers/addnewusers.component';
import { SigninComponent } from './components/signin/signin.component';
import { SigninpinComponent } from './components/signinpin/signinpin.component';
import { AppComponent } from './app.component';
import { Component } from '@angular/core';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { ChatlogsComponent } from './components/chatlogs/chatlogs.component';
import { InvoicesNavComponent } from './components/invoices/invoices.component';
import { RequestComponent } from './components/request/request.component';
import { RefundComponent } from './components/refund/refund.component';

// Set route list : ระบุที่อยู่ของ route
const Url = UrlConfig;
const RoutesList: Routes = [
  { path: Url.Home, component: HomeComponent },
  { path: Url.Clients, component: ClientsComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Packages, component: PackagesComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Payments, component: PaymentsComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Reports, component: ReportsComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Signin, component: SigninComponent },
  { path: Url.Signinpin, component: SigninpinComponent },
  { path: Url.Logs, component: LogsComponent },
  { path: Url.changePassword, component: ChangePasswordComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Management, component: ManagementComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Storage, component: StorageComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Dashboard, component: DashboardComponent, canActivate: [AuthenticationGuard] },
  { path: Url.ChatLogs, component: ChatlogsComponent },
  //show form admin
  { path: Url.Users, component: UsersComponent },
  { path: Url.Addnewusers, component: AddnewusersComponent },
  { path: Url.Chats, component: ChatsComponent },
  { path: Url.ReportClients, component: ReportsclientsComponent },
  { path: Url.Chatbot, component:  ChatbotComponent},
  { path:Url.invoices, component:InvoicesNavComponent, canActivate:[AuthenticationGuard]},
  { path:Url.request, component:RequestComponent, canActivate:[AuthenticationGuard]},
  { path: Url.VeeamBackup, component: VeeamBackupComponent, canActivate: [AuthenticationGuard] },
  { path: Url.VeeamReplication, component: VeeamReplicationComponent, canActivate: [AuthenticationGuard] },
  { path: Url.Refund,component:RefundComponent,canActivate: [AuthenticationGuard]}
  
];

export const RoutingModule = RouterModule.forRoot(RoutesList);