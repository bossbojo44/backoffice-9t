import { ChangePasswordComponent } from './components/change-password/change-password.component';
import { SharedService } from './services/shared.service';
import { VerifyService } from './services/verify.service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpModule } from '@angular/http';
import { RoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { HttpService } from './services/http.service';
import { HomeComponent } from './components/home/home.component';
import { SecurityService } from './services/security.service';
import { AuthenticationService } from './services/authentication.service';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ValidationDirective } from './directives/validation.directive';
import { LoadingDirective } from './directives/loading.directive';
import { LanguageService } from './services/language.service';
import { LanguagePipe } from './pipes/language.pipe';
import { SelectDirective } from './directives/helpers.directive';
import { PackagesComponent } from './components/packages/packages.component';
import { PaymentsComponent } from './components/payments/payments.component';
import { ReportsComponent } from './components/reports/reports.component';
import { ClientsComponent } from './components/clients/clients.component';
import { UsersComponent } from './components/users/users.component';
import { AddnewusersComponent } from './components/addnewusers/addnewusers.component';
import { ReportsclientsComponent } from './components/clients/reports/reports.component';
import { ChatsComponent } from './components/chats/chats.component';
import { SigninComponent } from './components/signin/signin.component';
import { SigninpinComponent } from './components/signinpin/signinpin.component';
import { ScrollbarDirective } from './directives/scrollbar.directive';
import { SignalRService } from './services/SignalR.service';
import { NotificationDirective } from './directives/notification.directive';
import { AuthenticationGuard } from './guard/authentication.guard';
import { LogsComponent } from './components/logs/logs.component';
import { ScrollvisibleDirective } from './directives/scrollvisible.directive';
import { ChatsService } from './components/chats/chats.service';
import { ChatbotComponent } from './components/chatbot/chatbot.component';
import { SignalRUserOnlineService } from './services/SignalR.UserOnine.service';
import { ManagementComponent } from './components/management/management.component';
import { StorageComponent } from './components/storage/storage.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { GoogleChartComponent } from './components/google-chart/google-chart.component';
import { ChatlogsComponent } from './components/chatlogs/chatlogs.component';
import { VeeamBackupComponent } from './components/packages/creates/veeam-backup/veeam-backup.component';
import { VeeamReplicationComponent } from './components/packages/creates/veeam-replication/veeam-replication.component';
import { NakivoBrComponent } from './components/packages/creates/nakivo-br/nakivo-br.component';
import { OnlyNumberDirective } from './directives/only-number.directive';
import { InvoicesNavComponent } from './components/invoices/invoices.component';
import { InvoicesComponent } from './components/packages/invoices/invoices.component';
import { RequestComponent } from './components/request/request.component';
import { RefundComponent } from './components/refund/refund.component';
import { InvoicecontrutorComponent } from './components/request/upgrade-package-request/invoicecontrutor/invoicecontrutor.component';
import { UpgradePackageRequestComponent } from './components/request/upgrade-package-request/upgrade-package-request.component';
import { UpgradeRequestComponent } from './components/request/upgrade-request/upgrade-request.component';
import { RequestHistoryComponent } from './components/request/request-history/request-history.component';
import { RequestNewpackageComponent } from './components/request/request-newpackage/request-newpackage.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { PaginationService } from './services/pagination.service';
import { PaginationPipe } from './components/pagination/pagination.pipe';
import { RequestService } from 'app/components/request/request.service';




@NgModule({
    declarations: [
        AppComponent,
        HomeComponent,
        NavbarComponent,
        ValidationDirective,
        LoadingDirective,
        SelectDirective,
        LanguagePipe,
        PackagesComponent,
        PaymentsComponent,
        ReportsComponent,
        ClientsComponent,
        UsersComponent,
        AddnewusersComponent,
        ReportsclientsComponent,
        ChatsComponent,
        SigninComponent,
        SigninpinComponent,
        ScrollbarDirective,
        NotificationDirective,
        LogsComponent,
        ScrollvisibleDirective,
        ChangePasswordComponent,
        ChatbotComponent,
        ManagementComponent,
        StorageComponent,
        DashboardComponent,
        GoogleChartComponent,
        ChatlogsComponent,
        VeeamBackupComponent,
        VeeamReplicationComponent,
        NakivoBrComponent,
        OnlyNumberDirective,
        InvoicesNavComponent,
        InvoicesComponent,
        RequestComponent,
        RefundComponent,
        InvoicecontrutorComponent,
        UpgradePackageRequestComponent,
        UpgradeRequestComponent,
        RequestHistoryComponent,
        RequestNewpackageComponent,
        PaginationComponent,
        PaginationPipe
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpModule,
        RoutingModule,
        BrowserAnimationsModule,
    ],
    providers: [
        HttpService,
        SecurityService,
        AuthenticationService,
        LanguageService,
        VerifyService,
        SharedService,
        SignalRService,
        ChatsService,
        AuthenticationGuard,
        SignalRUserOnlineService,
        PaginationService,
        RequestService
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }