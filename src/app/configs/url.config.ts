import { environment } from '../../environments/environment';
export const UrlConfig = {
    Home: '',
    Clients: 'clients',
    Packages: 'packages',
    Payments: 'payments',
    Reports: 'reports',
    Users: 'users',
    Addnewusers: 'addnewusers',
    Chats: 'portals-chat',
    Signin: 'signin',
    Signinpin: 'signinpin',
    ReportClients: 'rpt-clients',
    Logs:'logs',
    changePassword:'change-password',
    Chatbot: 'Chatbot',
    Management: 'management',
    Storage: 'storage',
    Dashboard: 'dashboard',
    ChatLogs: 'chat-logs',
    VeeamBackup: 'veeam-backup',
    VeeamReplication: 'veeam-replication',
    invoices:'invoices',
    request:'request',
    Refund:'refund'
};

export const baseUrl = environment.production ? 'http://test.9t.com' : 'http://localhost:38936';