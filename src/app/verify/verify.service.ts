import { Injectable } from '@angular/core';
import { HttpService } from '../services/http.service';

@Injectable()
export class VerifyService {

  constructor(private http: HttpService) { }

    private TempData: string;

    setTempData(TempData: string): void {
        this.TempData = TempData;
    }

    get getTempData(): string {
        return this.TempData || null;
    }


}
