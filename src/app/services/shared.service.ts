import { Injectable } from '@angular/core';

@Injectable()
export class SharedService {
  private _EmpId: number;
  
  constructor() { }

  get EmpId(): number {
    return this._EmpId;
  }
  set EmpId(EmpId: number) {
    this._EmpId = EmpId;
  }

}
