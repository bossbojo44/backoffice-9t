import { UrlConfig } from './../configs/url.config';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Injectable } from '@angular/core';
declare const $;
@Injectable()
export class SignalRUserOnlineService {

    private urlElement: HTMLScriptElement;
    private hub: any;
    private hubUrl: any;
    public connection: any;
    public client: any;
    public server: any;
    public connect: any;

    constructor(private authenticated: AuthenticationService, private router: Router) {
        this.hub = $.connection['userOnlineHub'];
        this.processScriptURL();
        if (!this.hub) {
            alert('userOnlineHub URL error');
            return;
        }

        this.connection = this.hub.connection;
        this.connection.url = this.hubUrl;
        this.server = this.hub.server;
        this.client = this.hub.client;
    }
    // connection Socket
    get getConnection() {
        return new Promise((resolve, reject) => {
            this.onProcessClient(this.client);
            this.connection.start().done(connect => {
                this.connect = connect;
                resolve(this.connect);
            });
        });
    }

    // disconnect Socket
    onDisconnection() {
        this.connect = null;
        this.connection.stop();
    }

    private processScriptURL() {
        let scriptURL = <HTMLScriptElement>document.getElementById('signleR-9t-url');
        if (scriptURL) this.hubUrl = scriptURL.src.replace('/hubs', '');
    }

    // callback from Socket
    private onProcessClient(client: any) {

        /*client.welcome = user => {
            console.log(user);
        };
        client.exit = (user) => {

            this.authenticated.destroyAuthenticatedByKey('Role');
            this.authenticated.destroyAuthenticatedByKey('Username');
            this.authenticated.destroyAuthenticatedByKey('');
            this.router.navigate(['/', UrlConfig.Signin]);
            alert('This user has been login!');
        };*/
        console.log("disable check login");
    }
}