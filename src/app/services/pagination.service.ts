import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
@Injectable()
export class PaginationService {
    private limit: number;

    setLimit(limit: any) {
        this.limit = parseInt(limit);
    }

    get getLimit() {
        return this.limit;
    }

    processPagination(items: any[], page, limit) {
        const itemsClone = Object.assign([], items);
        const itemFilter = itemsClone.filter((item, index) => {
            return (((page - 1) * this.limit) - 1) < index;
        });
        return itemFilter.splice(0, limit);
    }
}