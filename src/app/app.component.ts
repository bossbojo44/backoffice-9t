import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UrlConfig } from './configs/url.config';
import { SignalRUserOnlineService } from './services/SignalR.UserOnine.service';
import { AuthenticationService } from './services/authentication.service';
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    constructor(private router: Router, service: SignalRUserOnlineService, private authen: AuthenticationService) {
        /*service.getConnection.then(() => {
            if (authen.getAuthenticated)
                service.server.userConnect(authen.getAuthenticated);
        });*/
    }

    status: number;
    public role: any = '';

    onGetComponent(ev) {
        var pathname = location.pathname.trim();
        this.role = localStorage.getItem('Role');
        if (pathname == '/signin') {
            this.status = 0;
            if (this.role != null) {
                this.router.navigate(['/', UrlConfig.Clients]);
            }

        } else if (pathname == '/signinpin') {
            this.status = 0;
            if (this.role != null) {
                this.router.navigate(['/', UrlConfig.Clients]);
            }
        } else if (pathname == '/') {
            this.router.navigate(['/', UrlConfig.Signin]);
        } else {
            if (this.role === null) {
                this.router.navigate(['/', UrlConfig.Signin]);
            } else {
                this.status = 1;
            }
        }
    }
   unload(){
       /* this.authen.destroyAuthenticatedByKey('Role');
        this.authen.destroyAuthenticatedByKey('Username');
        this.authen.destroyAuthenticatedByKey(''); */
   }
}
