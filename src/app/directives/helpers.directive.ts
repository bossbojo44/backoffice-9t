import { Directive, HostBinding, HostListener, OnInit, ElementRef } from '@angular/core';
@Directive({
    selector: 'select.form-control'
})
export class SelectDirective implements OnInit {
    constructor(private elem: ElementRef) { }

    @HostBinding('attr.has-value') hasValue: boolean = false;

    @HostListener('change') onchange() {
        let input = <HTMLInputElement>this.elem.nativeElement;
        this.hasValue = input.value.trim() !== '';
    }

    ngOnInit() {
        let input = <HTMLInputElement>this.elem.nativeElement;
        this.hasValue = input.value.trim() !== '';
    }
}