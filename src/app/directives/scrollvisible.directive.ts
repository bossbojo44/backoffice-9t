import { Directive, AfterViewInit, Input, ElementRef } from '@angular/core';
declare let $;
@Directive({
  selector: '[ScrollV]'
})
export class ScrollvisibleDirective {
  @Input('ScrollV') hideCursor:any;
  constructor(private el: ElementRef) { }

  ngAfterViewInit() {
    $(this.el.nativeElement).niceScroll({
      cursorcolor: "#ccc",
      cursorwidth: "3px",
      cursorborder: "2px solid #ccc",
      zindex: "auto"
    });
  }

}
