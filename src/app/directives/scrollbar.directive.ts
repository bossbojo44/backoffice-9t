import { Directive, AfterViewInit, Input, ElementRef } from '@angular/core';
declare let $;
@Directive({
    selector: '[Scrollbar]'
})
export class ScrollbarDirective {

    @Input('Scrollbar') hideScorll: boolean = false;
    constructor(private el: ElementRef) {

    }

    ngAfterViewInit() {
        if (this.hideScorll) {
            $(this.el.nativeElement).niceScroll({
                cursorcolor: "#a0a0a0",
                horizrailenabled: false,
                cursorborder: 0,
                smoothscroll: true,
                hidecursordelay: 1000,
                oneaxismousemode: "auto",
                autohidemode: true
            });
        } else {
            $(this.el.nativeElement).niceScroll({
                cursorcolor: "#a0a0a0",
                horizrailenabled: false,
                cursorborder: 0,
                smoothscroll: true,
                hidecursordelay: 1000,
                oneaxismousemode: "auto",
                autohidemode: false
            });
        }
    }
}
